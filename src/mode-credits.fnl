(local credits {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(local credit-text
       "
Font (Operational Amplifier) - (OFL)
Font (Inconsolata) - (OFL)
Library (Lume) - RXI (MIT/X11)
Library (anim8,bump,gamera) - Enrique Cota (MIT)
Engine (LÖVE) - LÖVE Dev Team (Zlib)
Language (Fennel) - Calvin Rose (MIT/X11)")


(local elements
       [{:type :title :y 10 :w 400 :h 60 :text "CREDITS"}
        {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Back" }
        {:type :small-text :y 200 :oy -10 :ox 0 :w 900 :h 70 :text credit-text}])

(local element-font
       {:title  ((. assets.fonts "operational amplifier") 70)
        :button  ((. assets.fonts "operational amplifier") 40)
        :small-text  ((. assets.fonts "inconsolata") 20)})

(local element-click
       {"Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu") :credits))})

(local element-hover {:button (fn [element x y] :nothing)})


(local ui (buttons elements params element-click element-hover element-font))

(fn credits.draw [self]
  (ui:draw))

(fn credits.update [self dt]
  (ui:update dt))

(fn credits.mousereleased [self ...]
  (ui:mousereleased ...))

(fn credits.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

credits
