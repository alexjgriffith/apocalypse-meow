(local loader (require :lib.loader))

(fn [x y bump-reference] ;; passed in from map
  (local file "end-gate")
  (local [w h] [32 32])
  (local [sw2 vw2] [120 20])
  (local {: animations : image : grid : param} (loader file 16))
  (local gate {: animations
               : image
               : grid
               : param
               : bump-reference
               :pos {: x : y}
               :mod-colour [1 1 1 1]           :size {:w w :h h}
               :gate true
               :type :gate
               :active true
               :render true
               :state :loop
               :sprite true
               :name "GATE TO NEXT LEVEL"
               :at-gate false
               :next-level {:text "GO TO NEXT LEVEL" :x -20 :y -20}
               :save-cats {:text "SAVE CATS" :x 20 :y -20}
              ;; :stand-collider {:type :cat-stand :x (- x -16 sw2) :y (- y -16 sw2) :h (* 2 sw2) :w (* 2 sw2)}
              :next-level-collider {:type :next-level :x (- x -8 vw2) :y  (- y -8 vw2) :h (* 2 vw2) :w (* 2 vw2)}
              })

  gate)
