(local particle-image _G.assets.sprites.particle)

(fn init-rain []  
  (local rain-canvas (love.graphics.newCanvas 8 8))
  (local rain-quad (love.graphics.newQuad 0 0 8 8 16 16))
  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setCanvas rain-canvas)
  (love.graphics.draw particle-image rain-quad )
  (love.graphics.pop)
  rain-canvas)

(local rain {})

(fn rain.draw [self]
  (love.graphics.push :all)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setCanvas self.canvas)
  (love.graphics.clear 1 1 1 0)
  (love.graphics.draw self.rain 0 0)
  (love.graphics.setCanvas)
  (love.graphics.pop)
  self.canvas)

(fn rain.update [self dt]
  (self.rain:update dt))

(local rain-mt {:__index rain})

(fn rain.new [w? h?]
  (let [rain-canvas (init-rain)
        rain (love.graphics.newParticleSystem rain-canvas 1000)]
    ;; may run into issues with FS
    (local canvas (love.graphics.newCanvas (or w? 960) (or h? 700)))
    (rain:setParticleLifetime 2 5)
    (rain:setEmissionRate 5000)
    (rain:setSizeVariation 1)
    (rain:setEmissionArea :uniform (+ (or w? 960) 100) (+ (or h? 700) 100))
    (rain:setDirection 2)
    (rain:setSpeed 100 1000)
    (setmetatable
     {: rain
      :image rain-canvas
      :canvas canvas
      :active true
      :type :rain-particle
      }
     rain-mt)))

rain
