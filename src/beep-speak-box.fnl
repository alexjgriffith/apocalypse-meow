(local beep {})

(local text-box (require :lib.text-box))
(local timer (require :lib.timer))

(fn vowel? [letter]
  (local vowels
         {:A true :a true :E true :e true
          :I true :i true :O true :o true
          :U true :u true :Y true :y true})
  (. vowels letter))

(fn wordend? [letter]
  (or (=  letter " ")
      (=  letter "\n")
      (=  letter ",")
      (=  letter ".")))

(fn vowel-pitch [letter]
  (local vowels
         {:A 2 :a 2 :E 3 :e 3
          :I 4 :i 4 :O 0 :o 0
          :U 1 :u 1 :Y 5 :y 5})
  (or (. vowels letter) 0))

(fn beep.default-draw-box [self cx cy]
  (love.graphics.rectangle "line"
                           (- (+ cx self.x) self.padding)
                           (- (+ cy self.y) self.padding)
                           (+ self.w (* 3 self.padding))
                           (+ (* 2 self.padding) self.h)
                           ))

(fn beep.default-draw-scroll-bar [self]
  (local textbox self.textbox)
  (local current-height (* textbox.line textbox.font-height))
  (local visible-height textbox.visible-height)
  (local scroll-max (- textbox.box-height textbox.visible-height))
  (local ratio (/ visible-height current-height))
  (local scroll-bar-max (/ self.h 2))
  (local scroll-bar-position (* (- (/ self.h 2) self.padding) (/ self.scroll scroll-max)))
  (when (> self.scroll 0)
    (love.graphics.rectangle "line"
                             (+ self.x self.w self.padding)
                             (- self.y self.padding)
                             self.padding
                             (+ self.h (* 2 self.padding))
                             )
    (love.graphics.rectangle "fill"
                             (+ self.x self.w self.padding)
                             (+ self.y self.padding scroll-bar-position)
                             self.padding
                             scroll-bar-max
                             )))

(fn beep.update-text-canvas [self dt]
  (local textbox self.textbox)
  (local current-height (* textbox.line textbox.font-height))
  (local difference ( - current-height (+ textbox.visible-height self.scroll)))
  (local scroll-max (- textbox.box-height textbox.visible-height))
  (local scroll-rate (*  (+ 1 difference (* 4 textbox.font-height)) dt ))
  (tset self :scroll
        (math.max 0 (math.min scroll-max (+ self.scroll scroll-rate))))
  (local params (require :params))
  (love.graphics.setColor params.colours.white )
  (love.graphics.setCanvas self.text-canvas)
  (love.graphics.clear)
  (love.graphics.setColor self.font-color)
  (love.graphics.setFont self.font)
  (love.graphics.print self.textbox.written)
  (love.graphics.setCanvas))

(fn beep.clear-text-canvas [self]
  (love.graphics.setCanvas self.text-canvas)
  (love.graphics.clear)
  (love.graphics.setCanvas))

(fn beep.default-draw-text [self cx cy]
  (local quad (love.graphics.newQuad 0 self.scroll self.w self.h self.w self.textbox.box-height ))
  (love.graphics.draw self.text-canvas quad (+ self.x cx) (+ self.y cy)))

(fn beep.draw [self cx cy]
  (love.graphics.push "all")
  (local params (require :params))
  (love.graphics.setColor 1 1 1 1 )
  (when (not self.silent) (self:default-draw-box cx cy))
  ;; (self:default-draw-scroll-bar self.textbox)
  (self:default-draw-text cx cy)
  (love.graphics.pop))

(fn beep.make-sound [self letter]
  (local source (. self.sources self.source-index))
  (source.timer:reset)
  (source.source:seek (+ self.offset (* self.bpm60 (vowel-pitch letter))))
  (source.source:setPitch self.pitch)
  (source.source:setVolume 2)
  (source.source:play)
  (let [new-source-index (+ self.source-index 1)]
    (if (<= new-source-index self.number-of-sources)
        (set self.source-index new-source-index)
        (set self.source-index 1))))

(fn beep.stop-sounds [self]
  (each [_ source (ipairs self.sources)]
    (source.source:pause)))

(fn stop-old-sounds [self]
  (each [_ {:timer timer :source source} (pairs self.sources)]
    (when (> timer.time timer.end)
      (source:stop))))

(fn update-timers [self dt]
    (each [_ {:timer timer :source source} (pairs self.sources)]
      (timer:update dt)))

(fn beep.stop-sound [self which]
  (if which
      (: (. (. self.sources which) :source) :stop)
      (each [_ {:timer timer :source source} (pairs self.sources)]
        (source:stop))))

(fn in-word [letter last-letter]
  (match [(wordend? last-letter) (wordend? letter)]
    [false true] :end
    [true false] :start
    [false false] :in
    [true true] :out))

(fn beep.on-word-start [self letter peek]
  (match (in-word letter self.last-letter)
    :start (beep.make-sound self self.last-vowel)
    :end (beep.stop-sound self)))

(fn beep.on-vowel [self letter peek]
  (when (and (vowel? letter) (not (vowel? self.last-letter)))
    (beep.make-sound self letter)))

(fn beep.first-vowel-not-last [self letter peek]
    (match (in-word letter self.last-letter)
      :start (do
                 (beep.make-sound self self.last-vowel))
      :in  (when (and (vowel? letter)
                      (not (vowel? self.last-letter))
                      (not (wordend? self.last-last-letter))
                      (not (wordend? peek)))
             (beep.make-sound self letter))))

(fn beep.low-pass-on-word-end [self letter within-beat]
  (love.audio.setEffect :speakEffect
   {:type "distortion" :gain .5 :edge .25})
  (self.source:setEffect :speakEffect))

(fn beep.on-letter [self letter]
  (when (or (and (vowel? letter) (not (vowel? self.last-letter))))
    (set self.last-vowel letter))
  (beep.make-sound self.last-vowel))

(fn beep.update [self dt]
  (when self.active
    (local (catch letter peek) (self.textbox:update dt))
    (update-timers self dt)
    (local return
           (match catch
             :letter
             (do
                 (when (or (and (vowel? letter) (not (vowel? self.last-letter))))
                   (set self.last-vowel letter))
               (when (not self.silent)
                 (beep.on-vowel self letter peek)) ;; silent for now
               (set self.last-last-letter self.last-letter)
               (set self.last-letter letter)
               (beep.update-text-canvas self dt)
               :letter)
             :end :end
             _ :nothing
             ))
    (stop-old-sounds self)
    return))

(fn beep.new-textbox [self text]
  (local textbox (text-box self.font text self.w self.h))
  (local text-canvas (love.graphics.newCanvas self.w textbox.box-height))
  (tset self :textbox textbox)
  (tset self :text-canvas text-canvas)
  (tset self :scroll 0))

(fn beep.start [self] (self.textbox:start))
(fn beep.skip [self] (self.textbox:skip))
(fn beep.stop [self] (self.textbox:stop))
(fn beep.reset [self]
  (beep.stop-sounds self)
  (beep.clear-text-canvas self)
  (self.textbox:reset))

(local beep-mt
       {:__index beep
        :new-textbox beep.new-textbox
        :start beep.start
        :skip beep.skip
        :stop beep.stop
        :reset beep.reset
        :update beep.update
        :draw beep.draw})

(fn [x y w h text font source-file bpm60 play-sound?]
  (local number-of-sources 4)
  (local padding 15)
  (local sources [])
  ;; (local w 400)
  ;; (local h 400)
  (local textbox (text-box font text w h))
  (local text-canvas (love.graphics.newCanvas w textbox.box-height))
  (for [i 1 number-of-sources]
    (table.insert sources
                  {:source (love.audio.newSource source-file "static")
                   :timer (timer bpm60)}))
  (setmetatable
   {:number-of-sources number-of-sources
    :w w
    :h h
    :padding padding
    :x x
    :y y
    :silent (not play-sound?)
    :last-letter " "
    :last-last-letter "a"
    :last-vowel "a"
    :sources sources
    :source-index 1
    :bpm60 bpm60
    :font font
    :font-color white
    :offset 0
    :pitch 1.0
    :active true
    :textbox textbox
    :text-canvas text-canvas
    :scroll 0
    :freescroll false
    :freescroll-position 0
    :freescroll-rate 30
    }
   beep-mt))
