;; I should really tear out the ECS!! I am abusing it so much 😭
(local params (require :params))


(fn [level]
  {:level level ; reference-only
   :mouse {:tracking false :x 0 :y 0 :button 1}
   :keyboard {}
   :brush {:type :pen :brush-number 1 :brush :floor}
   ;; trigger saving on mousedown
   :history {:levels [] :max 20 :index 1}
   :editor true ;; needed to trigger the editor system
   :active true ;; set to false when in mode-game
   :render true
 })
