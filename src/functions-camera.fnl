(local tiny (require :lib.tiny))
(local gamera (require :lib.gamera))

(local free-camera {})
(tset free-camera :filter (tiny.requireAll "camera" "editor" "active"))


(local mid-camera {})
(tset mid-camera :filter (tiny.requireAll "camera" "player" "active"))


(local params (require :params))

(fn check-key-down [key]
  (if (-> params.keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn sign0 [x]
  (if (= x 0) 0
      (> x 0) 1
      (< x 0) -1))

(fn movement [camera dt]
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud (- (check-key-down :down) (check-key-down :up))
        speed camera.speed
        [x y] [(camera:getPosition)]
        pos {: x : y}
        max (if (and (~= 0 lr) (~= 0 ud)) params.speed.max-s2 params.speed.max)]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ params.speed.decay params.speed.rate) lr)
                     (* dt (sign0 speed.x) (- params.speed.decay)))
                  (- max) max))
    (set speed.y (lume.clamp
                           (+ speed.y
                              (* dt (+ params.speed.decay params.speed.rate) ud)
                              (* dt (sign0 speed.y) (- params.speed.decay)))
                           (- max) max))
    (when (and (< (math.abs speed.x) params.speed.floor) (= 0 lr) )
      (set speed.x 0))
    (when (and (< (math.abs speed.y) params.speed.floor) (= 0 ud))
      (set speed.y 0))
    (camera:setPosition (+ x speed.x) (+ y speed.y))
    (set pos.x (+ pos.x speed.x))
    (set pos.y (+ pos.y speed.y))))


(tset free-camera :process
      (fn [self camera dt]
        (movement camera dt)))

(tset mid-camera :process
      (fn [self camera dt]
        ;; (db :camera)
        ;; (local (mx my) (love.mouse.getPosition))
        ;; (local mxp (+ mx  camera.x ))
        ;; (local myp (+ my (- camera.y camera.h2)))
        (local [mxp myp] (cameraGetPosition self))
        (local entity (. (require :state) :entity :player))
        (local [w2 h2] [(/ entity.size.w 2) (/ entity.size.h 2)])
        (local [px py] [(+ entity.pos.x w2) (+ entity.pos.y h2)])
        ;; (pp camera)
        (camera:setPosition
         (+ px (- px mxp))
         (+ py (/ (- myp py) (* 0.65 3.2))))
                ))

{:free-camera free-camera : mid-camera}
