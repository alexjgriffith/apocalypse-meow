(local bump (require :lib.bump))

(local params (require :params))

(local timer (require :lib.timer))

(fn check-key-down [key]
  (if (-> params.keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn is-player-on-ground [player]
  (local (actual-x actual-y cols len)
         (player.collider.world:check player  player.pos.x (+ player.pos.y 1)
                                      (fn [item other]
                                         (match other.type
                                           :floor (do  :slide)
                                           _ :cross))))
  (set player.pos.x actual-x)
  (set player.pos.y actual-y)
  (> len 0))

(fn is-player-on-roof [player]
  (local (actual-x actual-y cols len)
         (player.collider.world:check player  player.pos.x (- player.pos.y 2)
                                      (fn [item other]
                                         (match other.type
                                           :floor (do  :slide)
                                           _ :cross))))
  (when (> len 0)
    (set player.speed.y 0)
    ;; (set player.pos.y actual-y)
    )
  ;; (set player.pos.x actual-x)

  (> len 0))

(fn is-jump-key-down [player]
  (= (check-key-down :up) 1))

(fn is-hook-down [player]
  (and player.hook-block (love.mouse.isDown 1)))

(fn is-jump-over [player]
  (player.controler.jump.timer:over))

(fn is-jump-released [player]
  player.controler.jump.released)

(fn is-jump-count<2 [player]
  (< player.controler.jump.count 2))

(fn is-jump-current-state [player]
  (local current-state player.state-machine.current-state)
  (= current-state :jump))

(fn jump-update [player dt]
  (player.controler.jump.timer:update dt))

(fn jump-reset [player dt]
  ;; do it like this so when params is updated so is the timer
  (set player.controler.jump.timer (timer params.player-speed.jump.timer))
  (tset player.controler.jump :count 0))

(fn jump-reset-timer [player dt]
  ;; do it like this so when params is updated so is the timer
  (player.controler.jump.timer:reset))

(fn jump-check-release [player]
  (when (not (is-jump-key-down))
    (set player.controler.jump.released true)))

(fn is-first-frame-mouse-down [player]
  (local ret (or player.controler.hook.first-frame (not player.controler.hook.released)))
  (tset player :controler :hook :first-frame false)
  (when (not player.controler.hook.released)
    (tset player :controler :hook :released (not (love.mouse.isDown 1))))
  ret)

(fn [player state dt]
  (local pos player.pos)
  (local current-state player.state-machine.current-state)

  ;; Update State
  (var next-state :fall)
  (let [on-ground (is-player-on-ground player)
        on-roof (is-player-on-roof player)
        hook-down (is-hook-down player)
        jump-key-down (is-jump-key-down player)
        jump-over (is-jump-over player)
        jump-released (is-jump-released player)
        jump-count<2 (is-jump-count<2 player)
        jump-current-state (is-jump-current-state player)
        first-frame-mouse-down (is-first-frame-mouse-down player)]
    (when on-ground
      (set next-state :walk))
    (when (and (not on-ground) (or jump-over (not jump-key-down)))
      (set next-state :fall))
    (when (and (and jump-key-down (not jump-over))
               (or jump-current-state jump-released)
               (or jump-current-state jump-count<2)
               (not on-roof))
      (set next-state :jump))
    (when (and hook-down (not first-frame-mouse-down))
      (set next-state :hook))
    )

  ;; Triggers when state changes
  (when (~= next-state player.state-machine.current-state)
    (match current-state
      :jump (jump-reset-timer player dt)
      :fall :nothing
      :walk :nothing
      :hook :nothing)
    (match next-state
      :jump (do
                ;; reset timers
                ;; increment jump.count
                (set player.controler.jump.count (+ player.controler.jump.count 1))
                (set player.controler.jump.released false)
                (set player.speed.y -20)
              )
      :fall (do :nothing)
      :walk (do (jump-reset player dt))
      :hook (do (jump-reset player dt))))

  ;; Global Updates
  (jump-check-release player)
  ;; State Specific Updates
  (match next-state
      :jump (jump-update player dt)
      :fall (do :nothing)
      :walk (do :nothing)
      :hook (do :nothing))

  (set player.state-machine.current-state next-state)
  ;; return previous state
  current-state)
