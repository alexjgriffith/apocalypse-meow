(local bump-plugin (require :bump-plugin))
(local gamestate (require :lib.gamestate))


(local corners {:center [0 0]
                :up [0 -1]
                :up-right [1 -1]
                :up-left [-1 -1]
                :down [0 1]
                :down-right [1 1]
                :down-left [-1 1]
                :left [-1 0]
                :right [1 0]})

(fn query-floor-callback [other]
  (match other.type
    :floor true
    _ false))

(fn query-block [world x y tile-size ?w ?h]
  (local w (or ?w 1))
  (local h (or ?h 1))
  (world:queryRect (* x tile-size)
                   (* y tile-size)
                   (* w tile-size)
                   (* h tile-size)
                   query-floor-callback))

(fn add-collider [world tile level]
  (bump-plugin.add-subtile world {} tile.id tile level.tile-size 1 0))

(fn remove-collider [world col]
  (when (world:hasItem col)
    (world:remove col)))

(fn remove-colliders [world cols]
  (when cols
    (each [_ col (pairs cols)]
    (remove-collider world col))))

(fn add-neighbour-colliders [world tile level]
  (local map (require :map))
  (each [key neighbour (pairs (map.get-neighbours tile.type level tile.x tile.y tile.l))]
    (when (= 1 neighbour)
      (local [dx dy] (. corners key))
      ;;(local neighbour-tile (lume.clone tile))
      ;;(tset neighbour-tile :x (+ tile.x dx))
      ;;(tset neighbour-tile :y (+ tile.y dy))
      (local index (map.xyl-to-id (+ tile.x dx) (+ tile.y dy) nil level.width))
      (local neighbour-tile (. level :data :ground index))
      (add-collider world neighbour-tile level))
    ;; (add-collider world (. level :data :ground neighbour) level)
    ))

(fn query-tile [world tile tile-size]
  (local { : x : y} tile)
  (query-block world x y tile-size))

(fn update-tile-colliders [world tile tile-size level delete?]
  (local { : x : y} tile)
  (each [key [dx dy] (pairs corners)]
    (remove-colliders world (query-block world (+ x dx) (+ y dy) tile-size)))
  (when (not delete?)
    (add-collider world tile level))
  (add-neighbour-colliders world tile level))


(fn add-cat [tile tile-size level]
  ;; add to entity cats
  (local cats (. (require :state) :entity :cats))
  (local ces (. (require :state) :ces))
  (local bump-reference (. (require :state) :entity :bump-world))
  (local cat ((require :prefab-cat) tile.sprite (* tile-size tile.x) (* tile-size tile.y) tile.flipped bump-reference))
  (table.insert cats cat)
  (ces:addEntity cat)
  (ces:refresh))


(fn remove-cat [tile tile-size level]
  (local cats (. (require :state) :entity :cats))
  (local ces (. (require :state) :ces))
  (var index 0)
  (each [key cat (pairs cats)]
    (when (and (= cat.pos.x (* tile.x tile-size)) (= cat.pos.y (* tile.y tile-size)))
      (set index key)))
  (local cat (. cats index))
  (ces:removeEntity cat)
  (ces:refresh)
  (tset cats index nil))

(fn add-player [tile tile-size level]
  (local new-objs {})
  (each [key value (pairs level.data.objs)]
    (when (or (~= :player value.type) (and (= tile.y value.y) (= tile.x value.x)))
      (tset new-objs key value)))

  (local state (require :state))
  (tset state :entity :level :level :data :objs new-objs)
  (local entity (. state :entity))
  (local ces (. (require :state) :ces))
  (local bump-reference (. (require :state) :entity :bump-world))
  (bump-reference.world:remove entity.player)
  (ces:removeEntity entity.player)
  (local player ((require :prefab-player)
                 (+ 16 (* tile-size tile.x))
                 (+ 24 (* tile-size tile.y))
                 8 8 tile.flipped bump-reference {}))
  (tset entity :player player)
  (ces:addEntity player)
  (bump-reference.world:update player player.pos.x player.pos.y)
  ;;(bump-reference.world:add entity.player)
  (ces:refresh))


(fn add-gate [tile tile-size level]
  (local new-objs {})
  (each [key value (pairs level.data.objs)]
    (when (or (~= :gate value.type) (and (= tile.y value.y) (= tile.x value.x)))
      (tset new-objs key value)))

  (local state (require :state))
  (tset state :entity :level :level :data :objs new-objs)
  (local entity (. state :entity))
  (local ces (. (require :state) :ces))
  (local bump-reference (. (require :state) :entity :bump-world))
  (ces:removeEntity entity.gate)
  (local gate ((require :prefab-gate)
                  (* tile-size tile.x)
                 (* tile-size tile.y)
                  bump-reference))
  (tset entity :gate gate)
  (ces:addEntity gate)
  ;;(bump-reference.world:add entity.player)
  (ces:refresh))


;;love.handlers[name](a,b,c,d,e,f)
(fn love.handlers.edit-map [instruction tile tile-size level]
  ;; should just call a system! ces.systems.map.edit
  (local state (require :state))
  (local bump-world state.entity.bump-world)
  ;; (local level state.entity.level)
  (when (and tile (= :floor tile.type) bump-world level)
    (local world bump-world.world)
    (match instruction
      :add-tile (update-tile-colliders world tile tile-size level)
      :remove-tile (update-tile-colliders world tile tile-size level :delete)
      ))

  (when (and tile (= :cat tile.type) bump-world level)
    (local world bump-world.world)
    (match instruction
      :add-tile (add-cat tile tile-size level)
      :remove-tile (remove-cat tile tile-size level)
      )
    )

  (when (and tile (= :player tile.type) bump-world level)
    (local world bump-world.world)
    (match instruction
      :add-tile (add-player tile tile-size level)
      )
    )

  (when (and tile (= :gate tile.type) bump-world level)
    (local world bump-world.world)
    (match instruction
      :add-tile (add-gate tile tile-size level)
      )
    )

  )

(fn love.handlers.animation-state-change [previous-state state]
  (match state
    :land (when assets
            (assets.sounds.land:setPitch 1)
            (assets.sounds.land:play))
    )
  )

(fn love.handlers.cat-near []
  ;;  make mew sound
  (when assets
    (assets.sounds.meaw:play))
  )

(fn love.handlers.cat-saved [name]
  ;;  make mew sound
    (when assets
      (assets.sounds.page:play))
  ;; increment score for this level
    (local state (require :state))
    (: (. state :entity :ui :save-declaration-timer) :reset)
    (table.insert (. state.levels state.level :saved-names) name)
    (tset (. state.levels state.level) :most-recently-saved name)
    (tset (. state.levels state.level) :saved (+ (. state.levels state.level :saved) 1))
  )


(fn love.handlers.water-hits [type x y]
  (local cats (. (require :state) :entity :cats))
  (match type
    :player (do (assets.sounds.scratch:play) (restart-level))
    :cat (each [index cat (pairs cats)]
           ;; avoid race conditions
           (local player (. (require :state) :entity :player))
           (when (and (= x cat.pos.x) (= y cat.pos.y) (~= player.pos.y y))
             (tset cats index :alive false)
             (tset cats index :dead true)
             (assets.sounds.hiss:play)
             (local state (require :state))
             (tset (. state.levels state.level) :lost (+ (. state.levels state.level :lost) 1))
             )
           )
    ))

(fn love.handlers.player-at-gate []
  "Display scores and let them press continue to go to the next level."
  ;;(pp :at-gate)
  :gate
  )

(fn love.handlers.next-level []
  (local level (. (require :state) :entity :level :level :next-level))
  (assets.sounds.scratch:play)
  (if level
      (start-level level)
          (gamestate.switch (require :mode-end-game))))
