(fn time-section-to-string [section]
  (if (< section 10)
      (string.format "0%s" section)
      (string.format "%s" section)))

(fn seconds-to-time [seconds]
  (local s (time-section-to-string (math.floor (% seconds 60))))
  (local m (time-section-to-string (math.floor (% (/ seconds 60) 60))))
  (string.format "%s:%s" m s))
