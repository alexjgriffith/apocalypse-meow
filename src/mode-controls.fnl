(local controls {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(local controls-text
       "
Look around with the mouse
Hook by pressing down the RMB
Release by lifting the RMB
Move left and right using the arrow keys or A/D
Jump using Up/ A or Space
F10   - Toggle Fullscreen
m     - Mute
p     - Pause
q     - Screenshot
Esc   - Return to the menu
Enter - Enter editor mode")


(local elements
       [{:type :title :y 10 :w 400 :h 60 :text "CONTROLS"}
        {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Back" }
        {:type :small-text :y 200 :oy -10 :ox 0 :w 900 :h 70 :text controls-text}])

(local element-font
       {:title  ((. assets.fonts "operational amplifier") 70)
        :button  ((. assets.fonts "operational amplifier") 40)
        :small-text  ((. assets.fonts "inconsolata") 20)})

(local element-click
       {"Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu") :controls))})

(local element-hover {:button (fn [element x y] :nothing)})


(local ui (buttons elements params element-click element-hover element-font))

(fn controls.draw [self]
  (ui:draw))

(fn controls.update [self dt]
  (ui:update dt))

(fn controls.mousereleased [self ...]
  (ui:mousereleased ...))

(fn controls.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

controls
