(local example  {})

;; sizes to support
;; 135 240.0
;; 270 480.0
;; 540  960.0
;; 1080 1920
;; 720 1280.0
;; 2160 3840.0

(local lipsum (require :lib.lipsum))

(local sample-text-1 lipsum.short-para)
(local sample-text-2 lipsum.one-para)

(local beep-speak-box ((require :beep-speak-box)
                       40 30
                       sample-text-1
                       (assets.fonts.inconsolata 16)
                       "assets/sounds/F# Beep Scale.wav"
                       (* 4 (/ 60.0 280))))

(local gif ((require :lib.save-gifs) "example"))

(fn draw-background [w h]
  (love.graphics.push "all")
  (love.graphics.setColor (/ 62 256) (/ 65 256) (/ 95 256) 1 )
  (love.graphics.rectangle "fill" 0 0 w h)
  (love.graphics.pop))

(var which-text 0)
(fn example.update [self dt]
  (gif:update dt)
  (match which-text
    0 (match (beep-speak-box:update dt)
           :end (do (beep-speak-box:reset)
                  (beep-speak-box:new-textbox sample-text-2)
                  (set which-text 1)))
    1 (match (beep-speak-box:update dt)
            :end (do (beep-speak-box:reset)
                   (beep-speak-box:new-textbox sample-text-1)
                   (set which-text 0)))))

(local samuel-quad (love.graphics.newQuad 0 0 16 32 32 32))
(fn example.draw [self]
  (let [(window-width window-height  _) (love.window.getMode)]
    (draw-background window-width window-height)
    (beep-speak-box:draw))
  (love.graphics.setColor 1 1 1 1)
  
  (love.graphics.draw assets.sprites.Samuel samuel-quad 200 170 0 2 ))

(fn example.init [self]
  (local (w h flags) (love.window.getMode))
  (set flags.minwidth 100)
  (set flags.minheight 100)
  ;;(love.window.setMode 480 270 flags)
  (love.window.setMode 960 540 flags)
  (beep-speak-box:start))


(fn example.keyreleased [_ code]
  (match code
    "c"   (gif:start)
    "s"   (gif:stop)
    "t"   (beep-speak-box:start)
    "p"   (beep-speak-box:stop)
    "r"   (beep-speak-box:reset)
    "n"   (beep-speak-box:skip)
    "escape" (love.event.quit)
    "return" (let [(w h opts) (love.window.getMode)]
                 (if opts.fullscreen
                     (love.window.setFullscreen false)
                     (love.window.setFullscreen true "desktop")))))


example
