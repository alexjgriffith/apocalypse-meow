(local state (require :state))
(local systems (require :systems))
(local params (require :params))
(local tiny (require :lib.tiny))

(fn load-editor-systems []
  (when state.ces.camera
    (state.entity.free-camera:setPosition state.ces.camera.x state.ces.camera.y)
    )
  (tset state :ces :camera state.entity.free-camera)
  (tset state.ces :mousetracking false)
  (each [_ system (ipairs systems.editor.update)]
    (tiny.addSystem state.ces system))
  (systems.setup-systems state.ces systems.editor :draw)
  (systems.setup-systems state.ces systems.editor :mousereleased)
  (systems.setup-systems state.ces systems.editor :mousepressed)
  (systems.setup-systems state.ces systems.editor :keypressed)
  (state.ces:refresh))

(fn remove-editor-systems []
   (state.ces:clearSystems )
  ;; (each [_ system (ipairs systems.editor.update)]
  ;;   (when (~= system.world nil)
  ;;     (tiny.removeSystem state.ces system)))
  (systems.remove-systems state.ces systems.editor :draw)
  (systems.remove-systems state.ces systems.editor :mousereleased)
  (systems.remove-systems state.ces systems.editor :mousepressed)
  (systems.remove-systems state.ces systems.editor :keypressed)
  (state.ces:refresh))

(fn load-player-systems []
  (tset state :ces :camera state.entity.mid-camera)
  (tset state.ces :mousetracking false)

  (each [_ system (ipairs systems.game.update)]
    (tiny.addSystem state.ces system))
  (systems.setup-systems state.ces systems.game :draw)
  (state.ces:refresh))

(fn remove-player-systems []
  (state.ces:clearSystems )
  ;; (each [_ system (ipairs systems.game.update)]
  ;;  (tiny.removeSystem state.ces system))
  (systems.remove-systems state.ces systems.game :draw)
  (state.ces:refresh))

(fn unload-current-level []
  (when state.ces
    (state.ces:clearSystems)
    (state.ces:clearEntities)
    (state.ces:refresh)
    (tset state :ces nil))
  (tset state :entity {}))

(var mute false)
(global toggle-sound
 (fn  []
   (if mute
       (do (love.audio.setVolume 1) (set mute false))
       (do  (love.audio.setVolume 0) (set mute true)))))

(global is-mute (fn [] mute))

(var mouselock true)
(global toggle-mouselock (fn [] (set mouselock (not mouselock))))
(global is-mouselock (fn [] mouselock))

(var mousevisible false)
(global toggle-mousevisible (fn [] (set mousevisible (not mousevisible))))
(global is-mousevisible (fn [] mousevisible))

(var fullscreen false)
(global is-fullscreen (fn [] fullscreen))
(var px 0)
(var py 0)
(global toggle-fullscreen
        (fn []
          (when (not _G.web)
             (local state (require :state))
             (local rain (require :prefab-rain))
          (if fullscreen
              (do (local (_W _h flags) (love.window.getMode))
                (local [w h] [params.screen-width params.screen-height])
                (tset flags :x px)
                (tset flags :y py)
                (tset flags :fullscreen false)
                (love.window.setMode w h flags)
                (state.rain.rain:release)
                (state.rain.canvas:release)
                (tset state :rain (rain.new))
                (set fullscreen false)
                )
              (do (local (w h flags) (love.window.getMode))
                (tset flags :fullscreen true)
                (set px flags.x)
                (set py flags.y)
                (state.rain.rain:release)
                (state.rain.canvas:release)
                (tset state :rain (rain.new))
                (love.window.setMode w h flags)
                (set fullscreen true))))))

(global screenshot (fn screenshot []
                     (when (not _G.web)
                       (love.graphics.captureScreenshot
                        (string.format "screenshot-%s.png" (os.time))))))

(var editor-state false)

(global toggle-editor
        (fn  []
          (match editor-state
            true (do
                     (remove-editor-systems)
                   (load-player-systems)
                   )
            false (do
                      (remove-player-systems)
                    (load-editor-systems)))
          (local (w h flags) (love.window.getMode))
          (local {: fullscreen} flags)
          (local state (require :state))
          (local cam state.ces.camera)
          (if fullscreen
              (cam:setWindow (math.floor (/ (- w  params.screen-width) 2)) 0
                                   params.screen-width h)
              (cam:setWindow 0 0 params.screen-width params.screen-height))
          (set editor-state (not editor-state))))

(global set-game
        (fn  []
          (match editor-state
            true (do
                     (remove-editor-systems)
                   (load-player-systems)
                   (set editor-state false)
                   ))))

(global set-editor
        (fn []
          (match editor-state
            false (do
                      (remove-player-systems)
                    (load-editor-systems)
                    (set editor-state true)))))

(global restart-level
        (fn []
          (local level-setups (require :level-setups))
          (unload-current-level)
          (match state.level
            "User Level" ((. level-setups state.level-type) state.level :new "User Level" "Complete")
            _ ((. level-setups state.level-type) state.level))
          (load-player-systems)))

(global start-level
        (fn [level level-type?]
          (unload-current-level)
          (local level-setups (require :level-setups))
          (local level-type (or level-type? :basic))
          (when (not state.levels)
            (tset state :levels {}))
          (tset state :level-type level-type)
          ((. level-setups level-type) level)
          (load-player-systems)))

(global new-level (fn [level level-type? ...]
                    (unload-current-level)
                    (local level-setups (require :level-setups))
                    (local level-type (or level-type? :basic))
                    (when (not state.levels)
                      (tset state :levels {}))
                    (tset state :level-type level-type)
                    ((. level-setups level-type) level :new ...)
                    (load-editor-systems)
                    ))

;; (global load-next-level (fn [] (new-level "New Level" :new "Level 2" "Complete")))

(global scale {})

(tset scale :270
      (fn []
        (local (w h flags) (love.window.getMode))
        (local state (require :state))
        (state.ces.camera:setScale 1.0)
        (state.ces.camera:setWindow 0 0 480 270)
        (love.window.setMode 480 270 flags)))

(tset scale :540
      (fn []
        (local (w h flags) (love.window.getMode))
        (local state (require :state))
        (state.ces.camera:setScale 2.0)
        (state.ces.camera:setWindow 0 0 960 540)
        (love.window.setMode 960 540 flags)))

(tset scale :720
      (fn []
        (local (w h flags) (love.window.getMode))
        (local state (require :state))
        (state.ces.camera:setScale 3.0)
        (state.ces.camera:setWindow 0 0 1280 720)
        (love.window.setMode 1280 720 flags)))

(tset scale :1080
      (fn []
        (local (w h flags) (love.window.getMode))
        (local state (require :state))
        (state.ces.camera:setScale 4.0)
        (state.ces.camera:setWindow 0 0 1920 1080)
        (love.window.setMode 1920 1080 flags)))

{ : new-level}
