(local tiny (require :lib.tiny))

(local water-system {})

(tset water-system :filter (tiny.requireAll "active" "water"))

(fn water-system.process [self entity args]
    (match (type args)
        :table ((. water-system self.world.callback) self entity
                (unpack args))
        _ ((. water-system self.world.callback) self entity args)))

(fn check-water-hits [other]
    (match other.type
      :player (do  (love.event.push :water-hits other.type other.pos.x other.pos.y) true)
      :cat (do  (love.event.push :water-hits other.type other.pos.x other.pos.y) true)
      _ false))

(fn water-system.c-update [self entity dt]
  ;; move water up
  (tset entity :level (math.max 0 (- entity.level entity.rate)))
  ;; check if anything is below the water level
  (let [x 0 y entity.level w 480 h entity.height]
    (local (items len) (entity.bump-reference.world:queryRect x (+ 40 y) w h check-water-hits))))

{ : water-system}
