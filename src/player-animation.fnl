(fn get-player-direction [self player]
  (local [mx my] (cameraGetPosition self))
  (local [w2 h2] [(/ player.size.w 2) (/ player.size.h 2)])
  (local [px py] [(+ player.pos.x w2) (+ player.pos.y h2)])
  (math.sin (math.atan2 (- py my) (- px mx)))
  )


(local animation-states
       [
        {
         :name :idle
         :state [:walk]
         :speed-x [:still]
         }
        {
         :name :talk
         :state [:walk]
         :speed-x [:still]
         :action [:talk]
         }
        {
         :name :walk
         :state [:walk]
         :speed-x [:walk :run]
         }
        {
         :name :run
         :state [:walk]
         :speed-x [:run]
         :facing-forward [true]
         }
        {:name :jump
         :state [:fall :jump]
         :speed-x [:still :walk :run]
         :speed-y [:rising]
         }

        {:name :jump-forward
         :state [:fall :jump]
         :speed-x [:run]
         :speed-y [:rising]
         :facing-forward [true]
         }
        ;; :jump-top     {:state [:fall :jump]
        ;;                :speed-y :still}
        {
         :name :fall
         :state [:fall :jump]
         :speed-x [:still :walk :run]
         :speed-y [:falling]
         }
        {
         :name :fall-forward
         :state [:fall :jump]
         :speed-x [:run]
         :speed-y [:falling]
         :facing-forward [true]
         }
        {:name :land
         :state [:walk]
         :previous-state [:jump :fall :hook]}
        {:name :hook :state [:hook]}])

(fn set-timer [frame]
  (local timer (require :lib.timer))
  (local psds (require :player-sprite-details))
  (timer (/ (. psds :frames frame :duration) 1000)))

(fn frame-count [state]
  (local timer (require :lib.timer))
  (local psds (require :player-sprite-details))
  (# (. psds :tags state :frames)))

(fn cycle-animation [player dt]
  "Repeat the cycle until a new animation state."
  (local {: state : previous-state  :frame [i _]} player.animation)
  ;; reset timer if entering animation state
  (when (~= state previous-state)
    (tset player :animation :frame-count (frame-count state))
    (tset player :animation :frame-offset 0)
    (tset player :animation :timer (set-timer i))
    (tset player :animation :over true)) ;; can end whenever it wants
  ;; Increment timer and check if it is over, if so increment frame offset
  (when (player.animation.timer:update dt)
    (tset player :animation :frame-offset (+ player.animation.frame-offset 1))
    (when (>= player.animation.frame-offset (- player.animation.frame-count 0)) ;; 0 indexed
      (tset player :animation :frame-offset 0))
    (tset player :animation :timer (set-timer (+ i player.animation.frame-offset)))))

(fn time-controled-animation [player dt]
  "The speed of the animation is dependent on how fast the player is moving in x"
  (cycle-animation player dt) ;; place holder
  )

(fn hold-animation [player dt]
  "Hold single frame until next animation state"
  (local {: state : previous-state} player.animation)
  ;; reset timer if entering animation state
  (when (~= state previous-state)
    (tset player :animation :frame-count 1)
    (tset player :animation :frame-offset 0)
    (tset player :animation :timer nil)
    (tset player :animation :over true))) ;; can end whenever it wants

(fn single-animation [player dt]
  "Force a single animation to play"
    (local {: state : previous-state  :frame [i _]} player.animation)
  ;; reset timer if entering animation state
  (when (~= state previous-state)
    (tset player :animation :frame-count (frame-count state))
    (tset player :animation :frame-offset 0)
    (tset player :animation :timer (set-timer i))
    (tset player :animation :over false)) ;; can't end until the animation is over
  ;; Increment timer and check if it is over, if so increment frame offset
  (when (player.animation.timer:update dt)
    (tset player :animation :frame-offset (+ player.animation.frame-offset 1))
    (when (>= player.animation.frame-offset (- player.animation.frame-count 1)) ;; 0 indexed
      (tset player :animation :frame-offset 0)
      (tset player :animation :over true))
    (tset player :animation :timer (set-timer (+ i player.animation.frame-offset)))))

(local animation-type
       {
        :cycle cycle-animation
        :time-controled time-controled-animation
        :hold hold-animation
        :single single-animation
        })

(fn member [tab element]
  (lume.reduce tab (fn [acc value] (or acc (= element value))) false))

(fn is-speed-x [speed]
  (if
   (= (math.abs speed) 0) :still
   (< (math.abs speed) 3) :walk
   :run))

(fn is-speed-y [speed]
  (if
   (> speed 0) :falling
   ;; (> speed -2) :still
   :rising))

(fn is-facing-forward [speed-x  player-x mouse-x]
  (if (or (and (< mouse-x player-x) (< speed-x 0))
          (and (>= mouse-x player-x) (>= speed-x 0))) true
       false))

(fn check-requirement [key callback ...]
  (if key
      (callback ...)
      true))

(fn check-state [requirements current-state]
  (check-requirement requirements.state member requirements.state current-state))

(fn check-previous-state [requirements previous-state]
  (check-requirement requirements.previous-state member requirements.previous-state previous-state))

(fn check-x-speed [requirements speed]
  (check-requirement requirements.x-speed is-speed-x speed))

(fn check-y-speed [requirements speed]
  (check-requirement requirements.y-speed is-speed-y speed))

(fn check-action [requirements action]
  (check-requirement requirements.action (fn [] (= requirements.action action))))

(fn check-snapshot [current-state previous-state speed-x speed-y action px mx]
  {:state current-state
   :previous-state previous-state
   :speed-x (is-speed-x speed-x)
   :speed-y (is-speed-y speed-y)
   :facing-forward (is-facing-forward speed-x px mx)
   :action action})

(fn next-animation [self player current-state previous-state mxp]
  (var next-animation-state :fall)
  (local checks (check-snapshot current-state previous-state player.speed.x player.speed.y
                                player.action player.pos.x mxp))
  (each [_ requirements
         (ipairs animation-states)]
    (when (and
           (member requirements.state current-state)
           (check-requirement requirements.previous-state member  requirements.previous-state previous-state)
           (check-requirement requirements.speed-x member requirements.speed-x checks.speed-x)
           (check-requirement requirements.speed-y member requirements.speed-y checks.speed-y)
           (check-requirement requirements.action member requirements.action checks.action)
           (check-requirement requirements.facing-forward member requirements.facing-forward checks.facing-forward))
      (set next-animation-state requirements.name)))
  next-animation-state)

(fn check-flip [[x y] flipped]
  (if (= 1 flipped)
      [x y]
      [(- x) y]))

(fn player-animation [self player current-state previous-state dt]
  (local player-sprite-details (require :player-sprite-details))
  (local [mxp myp] (cameraGetPosition self))
  ;; What is the current animation state
  (tset player :animation :previous-state player.animation.state)
  (when player.animation.over
    (tset player :animation :state (next-animation self player current-state previous-state mxp)))
  ;; signal when the state changes
  (when (~= player.animation.state player.animation.previous-state)
    (love.event.push :animation-state-change player.animation.previous-state player.animation.state))
  ;; Get the frame index
  (let [ang (get-player-direction self player)]
    (local angle (if
                  (> ang .9) 6
                  (> ang .4) 5
                  (> ang -0.4) 4
                  (> ang -0.85) 3
                  2
                  ))
    (tset player :animation :frame  [(. player-sprite-details :tags player.animation.state :frames 1)
                                     angle]))

  ;; Is the frame flipped
  (let  [[mxp myp] (cameraGetPosition self)
         [x y] [player.pos.x player.pos.y]]
    (tset player :animation :flipped (if (< mxp x) 1 -1)))

  ;; Determine the frame offset
  (let [state player.animation.state]
    ((. animation-type (. player-sprite-details :tags state :type)) player dt))

  ;; Update the beam offset
  (let [[i j] player.animation.frame
        frame-offset player.animation.frame-offset
        flipped player.animation.flipped]

  (tset player.animation :beam-offset
        (check-flip
         (. player-sprite-details :laser-offset
            (. player-sprite-details :frames (+ i frame-offset) :name) j)
         flipped)))
    )
