;; (require :lib.bump)

(local params (require :params))
(local timer (require :lib.timer))

(fn make-body-quads [x y tile-size image]
  (local tab [])
  (for [i 1 x]
    (local row [])
    (for [j 1 y]
      (tset row j
            (love.graphics.newQuad
             (* (+ i -1) tile-size) (* (- j 1) tile-size) tile-size tile-size
             (image:getWidth) (image:getHeight))))
    (tset tab i row)
    )
  tab)


(fn make-player [x y w h flipped bump-world collidables]
  (local image assets.sprites.Shadow)
  (local player {:pos {: x : y}
                 :speed {:x 0 :y 0}
                 :size {: w : h}
                 :angle 0
                 :action nil
                 :type :player
                 :image image
                 :quads (make-body-quads 40 100 32 image)
                 :animation {:timer nil
                             :flipped (if flipped -1 1)
                             :frame [0 0]
                             :frame-count 0
                             :frame-offset 0
                             :beam-offset [0 0]
                             :state :fall
                             :previous-state :fall
                             :over true
                             }
                 :controler {:jump
                             {:timer
                              (timer params.player-speed.jump.timer)
                              :count 0
                              :released true
                              }
                             :hook
                             {:first-frame true
                              :released false
                              }
                             }
                 :render true
                 :player true
                 :active true
                 :collider bump-world
                 :state-machine {:current-state :fall}
                 })
  (bump-world.world:add player x y w h)
  (table.insert collidables player)
  player)
