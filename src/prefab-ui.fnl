(local buttons (require :lib.buttons))
(local timer (require :lib.timer))

(local params (require :params))

(local element-font
       {:text  ( assets.fonts.inconsolata 20)
        :save-declaration ( assets.fonts.inconsolata 30)})

(fn [player]
  (local ui
         {:ui true
          :message ""
          :hud ""
          :save-declaration ""
          :save-declaration-timer (timer 2)
       :element-font element-font
       :active true
       :render true})
  (tset ui :player-reference player)
  ui)
