(local game {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))

(local params (require :params))
(local state (require :state))
(local systems (require :systems))

(var paused false)

(fn pause []
  (set paused (not paused)))

(var last-kb 0)
(var next-kb 0)
(fn draw-kb [] (love.graphics.push "all")
  (set next-kb (collectgarbage "count"))
  (love.graphics.setFont (assets.fonts.inconsolata 10))
    (love.graphics.print (.. "Memory actually used (in kB): "  (- next-kb last-kb)) 100 100)
    (love.graphics.print (.. "FPS: "  (love.timer.getFPS)) 100 120)
    (set last-kb next-kb)
    (love.graphics.pop))

(var lightning-timer 0)
(var next-lightning (- (math.random 6 10)))


(local debug-font (assets.fonts.inconsolata 10))
(fn draw-rain []
  (local (w h flags) (love.window.getMode))  
  (love.graphics.push :all)
  (love.graphics.setCanvas)
  (love.graphics.scale 1)
  (love.graphics.draw (state.rain:draw) 0 0)
  (love.graphics.setColor (/ 193 256) (/ 229 256) (/ 234 256) (* 0.1 lightning-timer))
  (love.graphics.rectangle :fill 0 0 w h)
  (love.graphics.pop))

(fn game.draw [self]
  (systems.process-systems state.ces :draw state.ces.camera)
  (draw-rain)
  (when profile (draw-kb)))

(var frames 0)
(fn game.update [self dt]
  ;; (love.timer.sleep (/ 1 10))
  (when profile
    (set frames (+ frames 1))
    (when (= 0 (% frames 100))
      (print (love.profiler.report "time" 20))
      (love.profiler.reset)))
  (when (and state.levels state.level (. state.levels state.level))
      (local level (. state.levels state.level))
  (when level.time
    (tset level :time (+ level.time dt))))
  (tset state.ces :callback :c-update)
  (when (not paused) (state.ces:update dt))
  (set lightning-timer (- lightning-timer dt))
  (pp lightning-timer)
  (when (< lightning-timer next-lightning)
    (set next-lightning (- (math.random 20 40)))
    (set lightning-timer 1)))

(fn game.leave [self ...]
 
  (love.mouse.setGrabbed false)
  (love.mouse.setVisible true))

(fn game.enter [self _ new-level?]
  
  (when (not (is-mousevisible))
    (love.mouse.setVisible false))
  (when (is-mouselock)
    (love.mouse.setGrabbed true))
  (if new-level?
    (match new-level?
      :new (do  (new-level "User Level" :basic "User Level" "Complete"))
      :resume (do
                (local (w h flags) (love.window.getMode))
                (local {: fullscreen} flags)
                (local cam state.ces.camera)
                (if fullscreen
                    (cam:setWindow (math.floor (/ (- w  params.screen-width) 2)) 0
                                   params.screen-width h)
                    (cam:setWindow 0 0 params.screen-width params.screen-height))
                :continue)
      _ (start-level new-level? :basic))
    (start-level "level2")))

(fn game.mousereleased [self x y button]
  (when (not paused)
    (systems.process-systems state.ces :mousereleased x y button)))

(fn game.mousepressed [self x y button]
  (when (not paused)
    (systems.process-systems state.ces :mousepressed x y button)))

(fn game.keypressed [self key code]
  (match key
    "escape" (gamestate.switch (require :mode-menu) :game)
    "return" (toggle-editor)
    "m" (toggle-sound)
    "q" (screenshot)
    "p" (pause)
    "r" (restart-level)
    "f10" (do (toggle-fullscreen)
              (local (w h flags) (love.window.getMode))
              (local {: fullscreen} flags)
            (local cam state.ces.camera)
            (if fullscreen
                (cam:setWindow (math.floor (/ (- w  params.screen-width) 2)) 0
                               params.screen-width h)
                (cam:setWindow 0 0 params.screen-width params.screen-height))
            ))
  (when (not paused)
    (systems.process-systems state.ces :keypressed key code)))

game
