(local game-over {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(var ui nil)

(local element-font
       {:title  ((. assets.fonts "operational amplifier") 100)
        :button  ((. assets.fonts "operational amplifier") 40)
        :small-text  ((. assets.fonts "inconsolata") 20)})

(local element-click
       {"New Game"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-new-game")))

        "Load Level"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-load")))})

(local element-hover {:button (fn [element x y] :nothing)})

(var beep-speak-box nil)

(var level-stat-box nil)

;; level1
;; 7 of 7 cats saved in 00:20
;;

(local ordered-levels ["level1" "level2" "level3" "level4" "level5" "level6" "level7" "level8" "level9"])
(fn game-over.enter []
  (local seconds-to-time (require :time))
  (local levels (. (require :state) :levels))
  (var cats-saved "\n\n")
  (var stats "\n\n")


  (each [_ name (ipairs ordered-levels)]
    (when (and levels (. levels name))
      (local level (. levels name))
      (set stats (.. stats name ": " level.saved " of "level.total-cats " in " (seconds-to-time level.time) "!\n\n"))
      (each [_ saved-name (ipairs (. level :saved-names))]
        (set cats-saved (.. cats-saved "\n" saved-name)))))

  (set beep-speak-box ((require :beep-speak-box)
                       550 150
                       400
                       400
                       cats-saved
                       (assets.fonts.inconsolata 20)
                       "assets/sounds/F# Beep Scale.wav"
                       (* 4 (/ 60.0 280))))

  (set level-stat-box ((require :beep-speak-box)
                       150 150
                       400
                       400
                       stats
                       (assets.fonts.inconsolata 20)
                       "assets/sounds/F# Beep Scale.wav"
                       (* 4 (/ 60.0 280))))

  (beep-speak-box:start)
  (level-stat-box:start)
  (local elements [{:type :title :y 20 :w 400 :h 80 :text "Cats You've Saved!"}
                   ;; {:type :small-text :y 150 :oy -10 :ox 0 :w 900 :h 70 :text cats-saved}
                   {:type :button :y 600 :oy -10 :ox -220 :w 400 :h 70 :text "New Game"}
                   {:type :button :y 600 :oy -10 :ox 220 :w 400 :h 70 :text "Load Level"}])
  (set ui (buttons elements params element-click element-hover element-font))
  )

(fn game-over.draw [self]
  (love.graphics.setColor 1 1 1 1)
  ;; (love.graphics.setFont (assets.fonts.inconsolata 20))
  ;; (love.graphics.printf stats 20 150 400 :left)
  (local [cx cy] (ui:c-offset))
  (match (level-stat-box:draw cx cy)
    :end (level-stat-box:reset))
  (match (beep-speak-box:draw cx cy)
    :end (beep-speak-box:reset))
  (ui:draw))

(fn game-over.update [self dt]
  (level-stat-box:update dt)
  (match (beep-speak-box:update dt)
    :end :nothing ;; (do (beep-speak-box:reset) ;; didn't like how this looked
    ;;   (beep-speak-box:start))
    )
  (ui:update dt))

(fn game-over.mousereleased [self ...]
  (ui:mousereleased ...))

(fn game-over.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

game-over
