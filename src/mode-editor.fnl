(local editor {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))

(local sprite-watcher ((require :sprite-watcher)))

(local params (require :params))
(local state (require :state))
(local systems (require :systems))

(var paused false)

(fn pause []
  (set paused (not paused)))

(fn screenshot []
  (love.graphics.captureScreenshot
   (string.format "screenshot-%s.png" (os.time))))

(fn editor.draw [self]
  (when (not paused)
    (systems.process-systems state.ces :draw state.ces.camera)))

(fn editor.update [self dt]
  (sprite-watcher:update dt)
  (tset state.ces :callback :c-update)
  (when (not paused)
    (state.ces:update dt)))

(fn editor.enter [self]
  ;;(start-level "test-level")
   (set-editor)
  )

(fn editor.mousereleased [self x y button]
  (when (not paused)
    (systems.process-systems state.ces :mousereleased x y button)))

(fn editor.mousepressed [self x y button]
  (when (not paused)
    (systems.process-systems state.ces :mousepressed x y button)))

(fn editor.keypressed [self key code]
  (match key
    "escape" (gamestate.switch (require :mode-menu) :editor)
    "return" (toggle-editor)
    "m" (toggle-sound)
    "q" (screenshot)
    "p" (pause)
    "r" (restart-level)
        "f10" (do (toggle-fullscreen)
                    (local (w h flags) (love.window.getMode))
                    (local {: fullscreen} flags)
                (local cam state.ces.camera)
                (if fullscreen
                    (cam:setWindow (math.floor (/ (- w  params.screen-width) 2)) 0
                                   params.screen-width h)
                    (cam:setWindow 0 0 params.screen-width params.screen-height))
                ))
  ;; v to save
  (when (not paused)
    (systems.process-systems state.ces :keypressed key code)))

editor
