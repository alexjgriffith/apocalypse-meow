(local menu {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))

(var has-entered-new-game false)

(var ui nil)

(local element-font
       {:title  ((. assets.fonts "operational amplifier") 100)
        :subtitle  ((. assets.fonts "operational amplifier") 40)
        :button  ((. assets.fonts "operational amplifier") 40)})

(local element-click
       {"Quit Game"
        (fn []
          (love.event.quit))

        "New Game"
        (fn []
          (set has-entered-new-game true)
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-new-game")))

        "New Level"
        (fn []
          (set-editor)
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") :new-level))

        "Continue"
        (fn []
          (set-game)
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") :resume))

        "Load Level"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-load")))

        "Editor"
        (fn []
          (assets.sounds.scratch:play)
          ;; (set-editor)
          (gamestate.switch (require "mode-editor")))

        "Controls"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-instructions")))

        "Credits"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-credits")))
        "Options"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-options"))
          )})

(local element-hover {:button (fn [element x y] :nothing)})


(fn menu.enter [self _ from]
  (local opening [{:type :title :y 70 :w 400 :h 60 :text "Apocalypse Meow"}
                  ;;{:type :title :y 170 :w 400 :h 60 :text "APOCALYPSE MEOW"}
                  {:type :subtitle :y 200 :w 400 :h 60 :text "( The Slightly Better Edition )"}
                  {:type :button :y 300 :oy -10 :ox 0 :w 400 :h 70 :text "New Game"}
                  {:type :button :y 400 :oy -10 :ox 0 :w 400 :h 70 :text "Load Level"}
                  {:type :button :y 500 :oy -10 :ox 0 :w 400 :h 70 :text "Options"}
                  {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Quit Game"}
                  ])
  (local standard [{:type :title :y 30 :w 400 :h 60 :text "Apocalypse Meow"}
                   {:type :button :y 200 :oy -10 :ox 0 :w 400 :h 70 :text  "Continue"}
                   {:type :button :y 300 :oy -10 :ox 0 :w 400 :h 70 :text "Editor"}
                   {:type :button :y 400 :oy -10 :ox 0 :w 400 :h 70 :text "Load Level"}
                   {:type :button :y 500 :oy -10 :ox 0 :w 400 :h 70 :text "Options"}
                   {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Quit Game"}])

  (local elements
       (if has-entered-new-game
           standard
           opening))
  (set ui (buttons elements params element-click element-hover element-font)))

(fn menu.draw [self]
  (ui:draw))

(fn menu.update [self dt]
  (ui:update dt ))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

(fn menu.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

menu
