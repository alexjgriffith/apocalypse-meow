(local params (require :params))

(fn check-key-down [key]
  (if (-> params.keys
          (. key)
          (lume.map love.keyboard.isDown)
          (lume.reduce (fn [a b] (or a b))))
      1
      0))

(fn sign0 [x]
  (if (= x 0) 0
      (> x 0) 1
      (< x 0) -1))

(fn movement-fall [player dt]
  (local ratio (* 60 dt)) ;; game was developed while running 60fps
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud (- (check-key-down :down) (check-key-down :up))
        speed player.speed
        speed-params params.player-speed.fall
        pos player.pos
        max (if (and (~= 0 lr) (~= 0 ud)) speed-params.max-s2 speed-params.max)]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed-params.decay speed-params.rate) lr)
                     (* dt (sign0 speed.x) (- speed-params.decay)))
                  (- max) max))
    ;; need to convert to verlet integration from euler
    (set speed.y (lume.clamp
                           (+ speed.y (* dt speed-params.gravity 1))
                           (- speed-params.fall-max) speed-params.fall-max))
    (when (and (< (math.abs speed.x) speed-params.floor) (= 0 lr) )
      (set speed.x 0))

    (local [goal-x goal-y] [(+ pos.x ( * ratio speed.x)) (+ pos.y ( * ratio speed.y))])
    (local (actual-x actual-y cols len)
           (player.collider.world:move player goal-x goal-y
                                       (fn [item other]
                                         (match other.type
                                           :floor :slide
                                           _ :cross))))
    (set pos.x actual-x)
    (set pos.y actual-y)
    ;; update-state-machine

    [cols len]))

(fn movement-walk [player dt]
  (local ratio (* 60 dt)) ;; game was developed while running 60fps
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud (- (check-key-down :down) (check-key-down :up))
        speed player.speed
        speed-params params.player-speed.walk
        pos player.pos
        max (if (and (~= 0 lr) (~= 0 ud)) speed-params.max-s2 speed-params.max)]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed-params.decay speed-params.rate) lr)
                     (* dt (sign0 speed.x) (- speed-params.decay)))
                  (- max) max))
    (when (and (< (math.abs speed.x) speed-params.floor) (= 0 lr) )
      (set speed.x 0))
    (set speed.y 0)
    (local [goal-x goal-y] [(+ pos.x ( * ratio speed.x)) (+ pos.y ( * ratio speed.y))])
    (local (actual-x actual-y cols len)
           (player.collider.world:move player goal-x goal-y (fn [item other]
                                                              (match other.type
                                                                :floor :slide
                                                                _ :cross))))
    (set pos.x actual-x)
    (set pos.y actual-y)
    [cols len]))

(fn movement-jump [player dt]
  (local ratio (* 60 dt)) ;; game was developed while running 60fps
  (let [lr (- (check-key-down :right) (check-key-down :left))
        ud (- (check-key-down :down) (check-key-down :up))
        speed player.speed
        speed-params params.player-speed.jump
        pos player.pos
        max  speed-params.max-s2
        jump-max speed-params.jump-max]
    (set speed.x (lume.clamp
                  (+ speed.x
                     (* dt (+ speed-params.decay speed-params.rate) lr)
                     (* dt (sign0 speed.x) (- speed-params.decay)))
                  (- max) max))
    ;; need to convert to verlet integration from euler    
    ;; (* ratio ratio 0.5 speed-params.gravity dt dt)
    (set pos.y (+ pos.y (* speed.y dt 60) (* speed-params.gravity 0.5 dt dt)))
    (set speed.y (lume.clamp
                  (+ speed.y (* dt (/ speed-params.gravity)))
                  (- jump-max) 0))    
    (when (and (< (math.abs speed.x) speed-params.floor) (= 0 lr) )
      (set speed.x 0))
    (when (and (< (math.abs speed.y) speed-params.floor) (= 0 lr) )
      (set speed.y 0))
    (local [goal-x goal-y] [(+ pos.x (* ratio speed.x))
                            ;;(+ pos.y (* ratio speed.y))
                            pos.y
                            ])
    (local (actual-x actual-y cols len)
           (player.collider.world:move player goal-x goal-y (fn [item other]
                                                              (match other.type
                                                                :floor :slide
                                                                _ :cross))))
    (set pos.x actual-x)
    (set pos.y actual-y)
    [cols len]))



(fn movement-hook [player dt]
  (local ratio  (* 60 dt)) ;; game was developed while running 60fps
  (let [;; down (love.mouse.isDown 1)
        speed player.speed
        speed-params params.player-speed.hook
        pos player.pos
        size player.size
        hook-block player.hook-block
        [hx hy] [(- hook-block.x (/ hook-block.w 2)) (- hook-block.y (/ hook-block.h 2)) ]
        [px py] [(- pos.x (/ size.w 2)) (- pos.y (/ size.h 2)) ]
        dx (if (= 0 (- px hx)) 0.00001 (- px hx))
        slope (math.abs (/ (- py hy )  dx))  ;; avoid division by 0
        mag (math.sqrt (+ 1 (* slope slope))) ;; expensive!!!!!!!!!!!
        ux (* (sign0 (- hx px)) (/ 1 mag))
        uy  (* (sign0 (- hy py)) (/ slope mag))
        ]
    (set speed.x
           (+ speed.x
              (* dt ux  speed-params.rate)))
    (set speed.y
           (+ speed.y
              (* dt uy  speed-params.rate)))
    (local speed-mag (math.sqrt (+ (* speed.x speed.x) (* speed.y speed.y))))
    (local speed-ux (math.abs (/ speed.x speed-mag)))
    (local speed-uy (math.abs (/ speed.y speed-mag)))
    (tset speed :x (lume.clamp speed.x
                               (- (* speed-params.max speed-ux))
                               (* speed-params.max speed-ux)))
    (tset speed :y (lume.clamp speed.y
                               (- (* speed-params.max speed-uy))
                               (* speed-params.max speed-uy)))
    (local [goal-x goal-y] [(+ pos.x ( * ratio speed.x)) (+ pos.y ( * ratio speed.y))])
    (local (actual-x actual-y cols len)
           (player.collider.world:move player goal-x goal-y
                                       (fn [item other]
                                         (match other.type
                                           :floor :slide
                                           _ :cross))))
    (set pos.x actual-x)
    (set pos.y actual-y)
    ;; update-state-machine

    [cols len]))

{ : movement-walk : movement-fall : movement-jump : movement-hook}
