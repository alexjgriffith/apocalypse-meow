(local tiny (require :lib.tiny))
(local map-editor (require :map-editor))
(local params (require :params))

(local map (require :map)) ;; needed for px-to-index

(local editor {})
(local save-map {})

(tset editor :filter (tiny.requireAll "editor" "active" (tiny.rejectAny"camera")))
(tset save-map :filter (tiny.requireAll "tiles" "active"))

;; Perhaps should refer to mouse keyboard object rather than level?
;; should replace camera here!
(fn get-mouse-pos [self {: level}]
  (map.xy-to-tile (cameraGetPosition self) level.tile-size))

;; GOD I LOVE THIS DESTRUCTURING SYNTAX!
;; if you want to edit the destructured value
;; make sure you pass in a table of a table
(fn hover [self {: level : brush}]
  (local [x y] (get-mouse-pos self level))
  (level.hover level brush.brush x y))

;; (fn right-click [self {: level : brush}]
;;   (local [x y] (get-mouse-pos self level))
;;   (level:remove-tile level.brush x y))

(fn backup-push [level history]
  ;; implement as a ring buffer, prevents us from having to
  ;; shift all levels? No the cost of shifting should not be
  ;; too great, when compared to the complexity of writing
  ;; a ring buffer (and debuging it 🙄)
  (when (> history.index history.max)
    (set history.index (- history.index 1))
    (each [index value (ipairs history.levels)]
      (when (~= index 1)
        (tset history.levels (- index)  value))))
  (tset history.levels history.index level)
  ;; should make a incf macro
  (set history.index (+ history.index 1)))

(fn backup-pop [level history]
  (when (> history.index 0)
    (set level.level (. history.levels history.index))
    (tset history.levels history.index nil)
    (set history.index (- history.index 1))
    level.level))

;; (fn undo [self editor-entity]
;;   (local {: }))

(fn pen [level brush x1 x2 y1 y2 delete?]
  (if (and (= x1 x2) (= y1 y2))
      (match delete?
        false (level:add-tile brush.brush x1 y1)
        true (level:remove-tile brush.brush x1 y1))
      (do
          (local slope (/ (- y2 y1) (- x2 x1)))
        (for [i (math.min x1 x2)  (math.max x1 x2)]
          (local rise (* slope i))
          (match delete?
            false (level:add-tile brush.brush i (math.floor (+ y1 rise)))
            true (level:remove-tile brush.brush i y1))
          ))))

(fn stamp [level brush x y delete?]
  (match delete?
    false (level:add-tile brush.brush x y)
    true (level:remove-tile brush.brush x y)))

(fn start-tracking [self {: level : history : mouse} button]
  (local [x y] (get-mouse-pos self level))
  ;; needed to comunicate between systems
  ;; only one tracker can be happening at once
  (backup-push level history)
  (set self.world.mousetracking true)
  (set mouse.tracking true)
  (set [mouse.x mouse.y mouse.button] [x y button]))

;; Should we do individual right and left mouse button tracking?
;; I'm thinking for now no...
(fn end-tracking [self {: level : mouse : brush} button]
  (local [x y] (get-mouse-pos self level))
  ;; What happens if we stop tracking for another
  ;; reason than lifting a button?
  (set self.world.mousetracking false)
  (match brush.type
    :stamp (stamp level brush x y (if (= 1 button) false true)))
  (set mouse.tracking false)
  (set [mouse.x mouse.y mouse.button] [x y button]))

(fn next-pointer [{: brush : level}]
  (tset brush :brush-number (+ 1 brush.brush-number))
  (when (> brush.brush-number (# level.brush-keys))
    (tset brush :brush-number  1))
  (tset brush :brush (. level.brush-keys brush.brush-number))
  (if (= brush.brush :floor) (tset brush :type :pen)
      (tset brush :type :stamp))
  )

;; update like this if you're going to have callbacks
(fn editor.process [self editor-entity args]
  ;; (db self.world.callback)
  (match (type args)
    :table ((. editor self.world.callback) self editor-entity (unpack args))
    _ ((. editor self.world.callback) self editor-entity args)))

(fn editor.c-update [self editor-entity dt]
  (local {: level : mouse : brush} editor-entity)
  (local [x1 y1] (get-mouse-pos self level))
  (when mouse.tracking
    (local [x2 y2 button] [mouse.x mouse.y mouse.button])
    ;; traverse the line from mx my x y and
    ;; draw tiles on each grid square that has not
    (match brush.type
      :pen (pen level brush x1 x2 y1 y2 (if (= 1 button) false true))
      :square nil
      :fill nil
      :stamp nil
    ))
  (set [mouse.x mouse.y] [x1 y1])
  ;; shouldnt hard code this like this
  ;; (tset editor-entity.level :states (. (require :neon-levels) :states))
  (tset editor-entity.level :tile-data (. (require :neon-levels) :tile-data))
  ;; (db level.level.data.objs)
  ;; needs to just be called with M-k
  (level:update level.level level.states)
  (hover self editor-entity))

(fn editor.mousepressed [self editor-entity x y button]
  (match button
    1 (start-tracking self editor-entity button)
    2 (start-tracking self editor-entity button)))

(fn editor.mousereleased [self editor-entity x y button]
  (match button
    1 (end-tracking self editor-entity button)
    2 (end-tracking self editor-entity button)))

(fn editor.keypressed [self editor key code]
  (local next-pointer-key (. params  :editor :keys :next-pointer))
  (match key
    next-pointer-key (next-pointer editor)
    "v" (if (love.filesystem.isFused)
            (editor.level:save (..
                                (love.filesystem.getSaveDirectory)
                                "/"
                                (or editor.level.level.name "new-level") ".fnl"))
            (editor.level:save (.. "src/" (or editor.level.level.name "new-level") ".fnl")))))

(fn save-map.process [self editor filename]
  (editor:save filename))

(tset save-map :active false)

{:editor editor
 :save-map save-map}
