{
  :data {
    :clouds {}
    :for1 {}
    :for2 {}
    :ground {
      3565 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 59
      }
      3566 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 59
      }
      3567 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 59
      }
      3568 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 59
      }
      3569 {
        :id 0
        :index [8 11 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 59
      }
      3629 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 60
      }
      3689 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 61
      }
      3749 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 62
      }
      3798 {
        :id 0
        :index [5 8 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 63
      }
      3799 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 63
      }
      3800 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 63
      }
      3801 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 63
      }
      3802 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 63
      }
      3803 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 63
      }
      3804 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 63
      }
      3805 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 63
      }
      3809 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 63
      }
      3858 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 64
      }
      3869 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 64
      }
      3905 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 65
      }
      3918 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 65
      }
      3929 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 65
      }
      3965 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 66
      }
      3978 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 66
      }
      3989 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 66
      }
      4025 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 67
      }
      4038 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 67
      }
      4085 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 68
      }
      4098 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 68
      }
      4145 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 69
      }
      4158 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 69
      }
      4205 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 70
      }
      4206 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 70
      }
      4207 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 70
      }
      4208 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 70
      }
      4209 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 70
      }
      4210 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 70
      }
      4218 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 70
      }
      4265 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 71
      }
      4278 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 71
      }
      4325 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 72
      }
      4338 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 72
      }
      4385 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 73
      }
      4398 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 73
      }
      4445 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 74
      }
      4454 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 74
      }
      4455 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 74
      }
      4456 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 74
      }
      4457 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 74
      }
      4458 {
        :id 0
        :index [4 2 3 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 74
      }
      4459 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 74
      }
      4460 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 74
      }
      4461 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 74
      }
      4462 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 74
      }
      4505 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 75
      }
      4518 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 75
      }
      4578 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 76
      }
      4638 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 77
      }
      4698 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 78
      }
      4758 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 79
      }
      4818 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 80
      }
      4878 {
        :id 0
        :index [6 2 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 81
      }
      4879 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 81
      }
      4880 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 81
      }
      4881 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 81
      }
      4882 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 81
      }
      4883 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 81
      }
      4884 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 81
      }
      4885 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 81
      }
      4886 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 81
      }
      4887 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 81
      }
      4888 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 81
      }
      4889 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 81
      }
      5103 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 85
      }
      5104 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 85
      }
      5105 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 85
      }
      5106 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 85
      }
      5107 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 85
      }
      5108 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 85
      }
      5109 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 85
      }
      5110 {
        :id 0
        :index [8 11 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 85
      }
      5170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 86
      }
      5230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 87
      }
      5290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 88
      }
      5346 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 89
      }
      5347 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 89
      }
      5348 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 89
      }
      5349 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 89
      }
      5350 {
        :id 0
        :index [4 12 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 89
      }
      5484 {
        :id 0
        :index [5 8 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 91
      }
      5485 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 91
      }
      5486 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 91
      }
      5487 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 91
      }
      5544 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 92
      }
      5604 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 93
      }
      5664 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 94
      }
      5724 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 95
      }
      5773 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 96
      }
      5774 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 96
      }
      5775 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 96
      }
      5776 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 96
      }
      5777 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 96
      }
      5778 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 96
      }
      5779 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 96
      }
      5784 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 96
      }
      5844 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 97
      }
      5904 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 98
      }
      5964 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 99
      }
      6024 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 100
      }
      6067 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 101
      }
      6068 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 101
      }
      6069 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 101
      }
      6070 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 101
      }
      6071 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 101
      }
      6072 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 101
      }
      6073 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 101
      }
      6074 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 101
      }
      6075 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 101
      }
      6076 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 101
      }
      6077 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 101
      }
      6078 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 101
      }
      6079 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 101
      }
      6080 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 101
      }
      6081 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 101
      }
      6082 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 101
      }
      6083 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 101
      }
      6084 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 101
      }
      6144 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 102
      }
      6204 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 103
      }
      6208 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 103
      }
      6209 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 103
      }
      6264 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 104
      }
      6324 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 105
      }
      6384 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 106
      }
      6420 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 107
      }
      6421 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 107
      }
      6422 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 107
      }
      6423 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 107
      }
      6424 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 107
      }
      6425 {
        :id 0
        :index [8 11 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 107
      }
      6444 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 107
      }
      6485 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 108
      }
      6504 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 108
      }
      6545 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 109
      }
      6564 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 109
      }
      6605 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 110
      }
      6624 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 110
      }
      6674 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 111
      }
      6684 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 111
      }
      6734 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 112
      }
      6744 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 112
      }
      6794 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 113
      }
      6804 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 113
      }
      6854 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 114
      }
      6864 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 114
      }
      6914 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 115
      }
      6924 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 115
      }
      6925 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 115
      }
      6926 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 115
      }
      6927 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 115
      }
      6928 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 115
      }
      6966 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 116
      }
      6967 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 116
      }
      6968 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 116
      }
      6969 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 116
      }
      6970 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 116
      }
      6971 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 116
      }
      6972 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 116
      }
      6973 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 116
      }
      6974 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 116
      }
      6984 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 116
      }
      7034 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 117
      }
      7044 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 117
      }
      7094 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 118
      }
      7104 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 118
      }
      7154 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 119
      }
      7164 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 119
      }
      7214 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 120
      }
      7224 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 120
      }
      7261 {
        :id 0
        :index [5 8 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 121
      }
      7262 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 121
      }
      7263 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 121
      }
      7264 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 121
      }
      7265 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 121
      }
      7266 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 121
      }
      7267 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 121
      }
      7268 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 121
      }
      7269 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 121
      }
      7270 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 121
      }
      7271 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 121
      }
      7272 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 121
      }
      7273 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 121
      }
      7274 {
        :id 0
        :index [4 2 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 121
      }
      7275 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 121
      }
      7276 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 121
      }
      7277 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 121
      }
      7278 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 121
      }
      7284 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 121
      }
      7321 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 122
      }
      7344 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 122
      }
      7381 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 123
      }
      7404 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 123
      }
      7441 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 124
      }
      7464 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 124
      }
      7501 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 125
      }
      7524 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 125
      }
      7561 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 126
      }
      7584 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 126
      }
      7621 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 127
      }
      7644 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 127
      }
      7681 {
        :id 0
        :index [6 2 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 128
      }
      7682 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 128
      }
      7683 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 128
      }
      7684 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 128
      }
      7685 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 128
      }
      7686 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 128
      }
      7687 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 128
      }
      7688 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 128
      }
      7689 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 128
      }
      7704 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 128
      }
      7764 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 129
      }
      7824 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 130
      }
      7884 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 131
      }
      7944 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 132
      }
      7987 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 133
      }
      7988 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 133
      }
      7989 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 133
      }
      7990 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 133
      }
      7991 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 133
      }
      7992 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 133
      }
      7993 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 133
      }
      7994 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 133
      }
      7995 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 133
      }
      7996 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 133
      }
      7997 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 133
      }
      7998 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 133
      }
      7999 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 133
      }
      8000 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 133
      }
      8001 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 133
      }
      8002 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 133
      }
      8003 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 133
      }
      8004 {
        :id 0
        :index [4 12 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 133
      }
    }
    :objs {
      6807 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 27
        :y 113
      }
      7145 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 5
        :y 119
      }
      7157 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 17
        :y 119
      }
      7564 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 4
        :y 126
      }
      7868 {
        :flipped false
        :h 32
        :id 0
        :image-h 224
        :image-w 704
        :l "objs"
        :library "sprite"
        :sprite "samuel"
        :type "player"
        :w 32
        :x 8
        :y 131
      }
      7882 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 22
        :y 131
      }
    }
    :sun {}
  }
  :height 34
  :id 0
  :tile-size 16
  :width 60
}
