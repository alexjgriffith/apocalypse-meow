(local menu {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(var ui nil)

(local elements  [ {:type :title :y 10 :w 400 :h 60 :text "LOAD LEVEL"}
                  {:type :button :y 125 :oy -10 :ox 220 :w 400 :h 70 :text "Level 1"}
                  {:type :button :y 225 :oy -10 :ox 220 :w 400 :h 70 :text "Level 2"}
                  {:type :button :y 325 :oy -10 :ox 220 :w 400 :h 70 :text "Level 3"}
                  {:type :button :y 425 :oy -10 :ox 220 :w 400 :h 70 :text "Level 4"}
                  {:type :button :y 525 :oy -10 :ox 220 :w 400 :h 70 :text "Level 5"}

                  {:type :button :y 125 :oy -10 :ox -220 :w 400 :h 70 :text "Level 6"}
                  {:type :button :y 235 :oy -10 :ox -220 :w 400 :h 70 :text "Level 7"}
                  {:type :button :y 325 :oy -10 :ox -220 :w 400 :h 70 :text "Level 8"}
                  {:type :button :y 425 :oy -10 :ox -220 :w 400 :h 70 :text "Level 9"}
                  {:type :button :y 525 :oy -10 :ox -220 :w 400 :h 70 :text "User Level"}
                  {:type :button :y 625 :oy -10 :ox 0 :w 400 :h 70 :text "Back"}])


(local element-font
       {:title  ((. assets.fonts "operational amplifier") 70)
        :button  ((. assets.fonts "operational amplifier") 40)})

(local element-click
       {"Level 1"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level1"))

        "Level 2"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level2"))

        "Level 3"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level3"))

        "Level 4"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level4"))

        "Level 5"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level5"))

        "Level 6"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level6"))

        "Level 7"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level7"))

        "Level 8"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level8"))

        "Level 9"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "level9"))

        "User Level"
        (fn []
          (assets.sounds.scratch:play)
          (gamestate.switch (require "mode-game") "user-level"))

        "Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu" :load)))})

(local element-hover {:button (fn [element x y] :nothing)})

(set ui (buttons elements params element-click element-hover element-font))

(fn menu.draw [self]
  (ui:draw))

(fn menu.update [self dt]
  (ui:update dt))

(fn menu.keypressed [self key code]
  (match key
    "escape"  (gamestate.switch (require :mode-end-game))
    "m" (toggle-sound)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

menu
