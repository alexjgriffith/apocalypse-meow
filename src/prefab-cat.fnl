(local loader (require :lib.loader))

(local adjectifs (lume.split assets.text.adjectifs "\n"))
(local base-cat-names (lume.split assets.text.base-cat-names "\n"))
(local subjects (lume.split assets.text.subjects "\n"))
(local titles (lume.split assets.text.titles "\n"))

(fn generate-cat-name []
  (fn rand-region [low high]
    (+ (* (math.random) (+ high (- low)))  low ))
  (local base-name (. base-cat-names (math.floor (rand-region 1 (# base-cat-names)))))

  (local adjectif
         (if (> (rand-region 1 5) 3)
             (. adjectifs (math.floor (rand-region 1 (# adjectifs))))
             false))

  (local subject
         (if (> (rand-region 1 5) 3)
             (. subjects (math.floor (rand-region 1 (# subjects))))
             false))
  (local title
         (if (> (rand-region 1 5) 3)
             (. titles (math.floor (rand-region 1 (# titles))))
             false))

  (var name base-name)
  (when adjectif (set name (.. name " the " adjectif)))
  (when (and adjectif subject) (set name (.. name " " subject)))
  (when title (set name (.. title " " name)))
  name)

(fn [file x y flipped bump-reference] ;; passed in from map
  (local [w h] [32 32])
  (local [sw2 vw2] [120 40])
  (local {: animations : image : grid : param} (loader file 32))
  (local cat {: animations
              : image
              : grid
              : param
              : bump-reference
              : flipped
              :name (generate-cat-name)
              :pos {: x : y}
              :mod-colour [1 1 1 1]
              :size {:w w :h h}
              :cat true
              :type :cat
              :alive true
              :active true
              :render true
              :saved false
              :state :wait
              :sprite true
              :stand-collider {:type :cat-stand :x (- x -16 sw2) :y (- y -16 sw2) :h (* 2 sw2) :w (* 2 sw2)}
              :save-collider {:type :cat-save :x (- x -16 vw2) :y  (- y -16 vw2) :h (* 2 vw2) :w (* 2 vw2)}
              })
  (let [sit cat.animations.sit
        sit-callback (fn [cat] (fn [] (tset cat :state :wait)))
        stand cat.animations.stand
        stand-callback (fn [cat] (fn [] (tset cat :state :active)))]
    (tset sit :onLoop (sit-callback cat))
    (tset stand :onLoop (stand-callback cat)))
  (bump-reference.world:add cat x y w h)
  cat)
