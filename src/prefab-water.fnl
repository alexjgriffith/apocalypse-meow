(local params (require :params))

(fn [active rate offset-y bump-reference]
  {: active
   : rate
   : bump-reference
   : offset-y
   :height params.map-height
   :water true
   :render true
   :level (+ params.map-height offset-y)
   }
  )
