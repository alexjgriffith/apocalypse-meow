(local tiny (require :lib.tiny))
;;(local {: new-level} (require :level-management))

(local gate-system {})

(tset gate-system :filter (tiny.requireAll "active" "sprite" "gate"))



(fn gate-system.process [self entity args]
    (match (type args)
        :table ((. gate-system self.world.callback) self entity
                (unpack args))
        _ ((. gate-system self.world.callback) self entity args)))


(fn check-gate-hits [other]
    (match other.type
      :player (do  (love.event.push :player-at-gate) true)
      _ false))


(fn gate-system.c-update [self entity dt]
  ;; move water up
  (: (. entity :animations :loop) :update dt)
  ;; (pp entity.animations)
  ;; check if anything is below the water level
  (let [{: x : y : w : h} entity.next-level-collider ]
    (local (items len) (entity.bump-reference.world:queryRect x y w h check-gate-hits))
    (if (> len 0)
        (tset entity :at-gate true)
        (tset entity :at-gate false)
        ))
  (let  [[mxp myp] (cameraGetPosition self)
         [x y] [entity.pos.x entity.pos.y]]
    (tset entity :choice-leave (< mxp x))
    (when (and (love.mouse.isDown 1) (< mxp x) entity.at-gate)
      (love.event.push :next-level))
  ))

{ : gate-system}
