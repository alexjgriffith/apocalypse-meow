(local bump (require "lib.bump"))

(fn [collidables]
  (local triggers {:world (bump.newWorld 32)
                   :name "triggers"
                   :render nil
                   :triggers true})
  triggers)
