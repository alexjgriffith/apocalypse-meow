{:frames {
          1 { :laser-offset [0 0] :duration 300 :name :default}
          2 { :laser-offset [0 0] :duration 300 :name :idle}
          3 { :laser-offset [0 0] :duration 100 :name :talk}
          4 { :laser-offset [0 0] :duration 100 :name :walk-front-plant}
          5 { :laser-offset [0 0] :duration 100 :name :walk-front-mid}
          6 { :laser-offset [0 0] :duration 100 :name :walk-front-release}
          7 { :laser-offset [0 0] :duration 100 :name :walk-back-plant}
          8 { :laser-offset [0 0] :duration 100 :name :walk-back-mid}
          9 { :laser-offset [0 0] :duration 100 :name :walk-back-release}
          10 { :laser-offset [0 0] :duration 100 :name :run-back-forward}
          11 { :laser-offset [0 0] :duration 100 :name :run-back-down}
          12 { :laser-offset [0 0] :duration 100 :name :run-front-forward}
          13 { :laser-offset [0 0] :duration 100 :name :run-front-down}
          14 { :laser-offset [0 0] :duration 100 :name :jump}
          15 { :laser-offset [0 0] :duration 100 :name :top-down}
          16 { :laser-offset [0 0] :duration 100 :name :top-up}
          17 { :laser-offset [0 0] :duration 100 :name :fall}
          18 { :laser-offset [0 0] :duration 100 :name :land-squash}
          19 { :laser-offset [0 0] :duration 100 :name :land-release}
          20 { :laser-offset [0 0] :duration 100 :name :jump-forward}
          21 { :laser-offset [0 0] :duration 100 :name :fall-forward}
          22 { :laser-offset [0 0] :duration 100 :name :hook}
          }
 :tags {
        :idle {:type :cycle :frames [1 2] :multiframe true}
        :talk {:type :cycle :frames [1 2] :multiframe true}
        :walk {:type :time-controled :frames [4 5 6 7 8 9] :multiframe true}
        :run {:type :time-controled :frames [10 11 12 13] :multiframe true}
        :jump {:type :hold :frames [14] :multiframe false}
        :jump-forward {:type :hold :frames [20] :multiframe false}
        :jump-top {:type :single :frames [15 16] :multiframe true}
        :land {:type :single :frames [18 19] :multiframe true}
        :fall {:type :hold :frames [17] :multiframe false}
        :fall-forward {:type :hold :frames [21] :multiframe false}
        :hook {:type :hold :frames [22] :multiframe false}
        }
 :laser-offset {:default            {2 [3 0]  3 [-3 -2]  4 [-5 -8] 5 [-3 -12] 6 [1 -14]}
                :idle               {2 [3 -1] 3 [-3 -3]  4 [-5 -9] 5 [-3 -13] 6 [1 -15]}
                :takk               {2 [3 0]  3 [-3 -2]  4 [-5 -8] 5 [-3 -12] 6 [1 -14]}

                :walk-front-plant   {2 [3 0]  3 [-3 -2]  4 [-5 -8] 5 [-3 -12] 6 [0 -14]}
                :walk-front-mid     {2 [4 0]  3 [-3 0]   4 [-5 -6] 5 [-3 -10] 6 [0 -12]}
                :walk-front-release {2 [3 1]  3 [-3 -1]  4 [-5 -7] 5 [-4 -10] 6 [0 -14]}
                :walk-back-plant    {2 [-1 0]  3 [-3 -2]  4 [-5 -8] 5 [-3 -12] 6 [0 -14]}

                :walk-back-mid      {2 [1 1]  3 [-3 -1]  4 [-5 -7] 5 [-3 -11] 6 [0 -13]}
                :walk-back-release  {2 [2 0]  3 [-3 -2]  4 [-5 -7] 5 [-3 -11] 6 [0 -14]}

                :run-back-forward  {2 [3 -4]  3 [-3 -4]  4 [-5 -10] 5 [-3 -14] 6 [0 -16]}
                :run-back-down     {2 [2 0]  3 [-2 -2]  4 [-5 -5] 5 [-5 -10] 6 [0 -14]}
                :run-front-forward {2 [3 -5]  3 [-3 -5]  4 [-5 -11] 5 [-3 -15] 6 [0 -17]}
                :run-front-down    {2 [2 0]  3 [-2 -3]  4 [-5 -6] 5 [-5 -11] 6 [0 -15]}

                :jump {2 [3 -3]  3 [-3 -5]  4 [-5 -11] 5 [-3 -15] 6 [0 -17]}

                :top-down {2 [3 0]  3 [-3 -2]  4 [-5 -8] 5 [-3 -12] 6 [0 -14]}

                :top-up {2 [3 0]  3 [-3 -2]  4 [-5 -8] 5 [-3 -12] 6 [0 -14]}

                :fall {2 [3 -3]  3 [-3 -5]  4 [-5 -11] 5 [-3 -15] 6 [0 -17]}

                :land-squash {2 [3 0]  3 [-3 2]  4 [-5 -2] 5 [-3 -6] 6 [0 -8]}
                :land-release {2 [3 0]  3 [-3 2]  4 [-5 -4] 5 [-3 -6] 6 [0 -8]}

                :jump-forward {2 [3 -5]  3 [-3 -8]  4 [-5 -12] 5 [-3 -16] 6 [0 -17]}

                :fall-forward {2 [3 -5]  3 [-3 -8]  4 [-5 -13] 5 [-3 -17] 6 [0 -17]}

                :hook {2 [3 -3]  3 [-3 -5]  4 [-5 -11] 5 [-3 -15] 6 [0 -17]}



          }

 }
