(local state (require :state))
(local systems (require :systems))
(local params (require :params))
(local tiny (require :lib.tiny))

;; This is where all the entities are added to the CES
;; NO SYSTEMS ALLOWED!
(fn basic [name new? ...]
  (tset state :entity {})

  (if (not new?)
      (tset state :entity :level ((require :prefab-map) name))
      (tset state :entity :level ((require :make-empty-map) ...)))
  (tset  state :entity :editor ((require :prefab-editor) state.entity.level))
  (tset state :entity :bump-world ((require :prefab-bump-world) {} state.entity.editor.level.level))
  (tset state :entity :cats [])
  (var player-data nil)
  (var gate-data nil)
  (each [_ value (pairs state.entity.level.level.data.objs)]
    (local tile-size state.entity.level.tile-size)
    (match value.type
      :cat (table.insert state.entity.cats
                         ((require :prefab-cat)
                          "cat-outline"
                          (* tile-size value.x)
                          (* tile-size value.y)
                          value.flipped
                          state.entity.bump-world))
      :player  (set player-data [(* tile-size value.x) (* tile-size value.y) 8 8 value.flipped state.entity.bump-world {}])
      :gate   (set gate-data [(* tile-size value.x) (* tile-size value.y)  state.entity.bump-world])
      ))
  (if (not player-data)
      (do (pp (string.format "No player defined in level %s!" name))
        (tset state :entity :player
              ((require :prefab-player) 240 -10 8 8 :flipped state.entity.bump-world {}))
        )
      (tset state :entity :player
            ((require :prefab-player) (unpack player-data))))

  (if (not gate-data)
      (do (pp (string.format "No gate defined in level %s!" name))
        (tset state :entity :gate
              ((require :prefab-gate) 240 100 state.entity.bump-world)))
      (tset state :entity :gate
            ((require :prefab-gate) (unpack gate-data))))

  (tset state :entity :water ((require :prefab-water) true (or state.entity.level.level.water-rate params.water-rate) 0 state.entity.bump-world))
  (tset state :entity :ui  ((require :prefab-ui) state.entity.player))
  (tset state :entity :free-camera ((require :prefab-free-camera) state.entity.editor))
  (tset state :entity :mid-camera ((require :prefab-mid-camera) state.entity.player))
  (tset state :ces
        (tiny.world
         state.entity.level
         state.entity.free-camera
         state.entity.mid-camera
         state.entity.editor
         state.entity.bump-world
         state.entity.ui
         state.entity.player
         state.entity.water
         state.entity.gate))
  (each [_ cat (ipairs state.entity.cats)]
    (state.ces:addEntity cat))
  ;; update scoring system
  (tset state :level name)
  (tset state :cats-in-this-level (# state.entity.cats))
  (tset state :levels name {:total-cats state.cats-in-this-level :saved 0 :lost 0 :saved-names [] :lost-names [] :most-recently-saved "" :time 0}))

{: basic}
