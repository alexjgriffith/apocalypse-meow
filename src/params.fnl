(local colour (require :lib.colour))
{:colours {
           :background (colour.hex-to-rgba "#2e2c3b");;"#3e415f")
           :white (colour.hex-to-rgba "#c1e5ea")
           :light-yellow (colour.hex-to-rgba "#c6de78")
           :blue (colour.hex-to-rgba "#3b77a6")
           }
 :water-rate 0.5
 :screen-width 960
 :screen-height 700 ;;540
 :map-width 480
 :map-height 4320
 :editor {:keys {:next-pointer :n :undo :/}}
 :keys {:left ["left" "a"]
        :right ["right" "d"]
        :up ["up" "w" "space"]
        :down ["down" "s"]}
 :scale 2
 :player-speed {
                :fall {:x 0 :y 0 :rate 7 :decay 7
                       :gravity  60
                       :fall-max 8
                       :max 4 :max-s2 (/ 6 (math.sqrt 2)) :floor 0.3}
                :walk {:x 0 :y 0 :rate 7 :decay 20
                       :max 4 :max-s2 (/ 4 (math.sqrt 2)) :floor 0.3}
                :jump {:x 0 :y 0 :rate 7 :decay 7
                       :timer 0.3
                       :gravity 10
                       :jump-max 8
                       :max 4 :max-s2 (/ 4 (math.sqrt 2)) :floor 0.3}
                :hook {:rate 100
                       :max 8}
                }
 :speed {:x 0 :y 0 :rate 7 :decay 20
         :max 4 :max-s2 (/ 3 (math.sqrt 2)) :floor 0.1}
 :editor-speed {:x 0 :y 0 :rate 1 :decay 1
                :max 20 :max-s2 (/ 1 (math.sqrt 2)) :floor 0.1}
 }
