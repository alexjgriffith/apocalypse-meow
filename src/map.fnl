(local subtile (require :lib.subtile))
(local map {})

;; this should be wrapped into a library with subtile and
;; some of the logic from prefab-map


(fn map.update-tileset-batch [batch tiles mapin tile-size layers? offset-x?]
    (: batch :clear)
    (local offset-x (or offset-x? 0))
    (local layers layers?);;(or layers? [:sun :clouds :ground :objs]))
    ;; subtiles are half the size of regular tiles
    ;; this should be updated to be a variable!!
    (each [_ level (pairs layers)]
      (each [id tile (pairs (. mapin :data level))]
        (if (= tile.library :subtile)
            (do
                (subtile.batch batch (/ tile-size 2)  tile.x  tile.y
                               (. tiles tile.type) tile.index offset-x))
            (= tile.library :tile)
            (subtile.batch-rect batch (/ tile-size 2)  tile.x  tile.y
                                (. tiles tile.type) tile.w
                                tile.h mapin.width offset-x)
            (= tile.library :sprite)
            (do
                (local quad (love.graphics.newQuad 0 0 32 32 tile.image-w tile.image-h))
                (batch:add quad  (+ offset-x (* (- tile.x 0) tile-size )) (* (+ tile.y 0) tile-size )))
            )))
    (: batch :flush))

;; this is the only function with reference to
;; scale and camera. It maps pixels on the screen
;; to tiles in the map.
(fn map.xy-to-tile [[x y] tile-size]
  [(math.floor (/ (- x 0) (* 1 tile-size)))
   (math.floor (/ (- y 0) (* 1 tile-size)))])

(fn map.xyl-to-id [x y l width height]
    (+ (* y width) x))

(fn map.tile [type library ?w ?h ?image-w ?image-h ?sprite ?flipped]
    (var tile {:library library :type type})
    (when (or (= library :tile) (= library :sprite))
      (tset tile :w ?w)
      (tset tile :h ?h))
    (when  (= library :sprite)
      (tset tile :sprite ?sprite)
      (tset tile :image-w ?image-w)
      (tset tile :image-h ?image-h)
      (tset tile :flipped (or ?flipped (if (= 1 (math.floor (lume.random 0 2))) true false)))
      )
    tile)

(fn map.new [width height levels tile-size]
    (let [ret { :sun {} :clouds {} :ground {} :objs {} :for1 {} :for2 {}}]
      {:data ret :width width :height height :id 0 :tile-size tile-size}))

(fn map.add-tile [mapin x y l tile ?details]
    (var index (map.xyl-to-id x y l mapin.width mapin.height))
    (when tile
      (tset tile :x x)
      (tset tile :y y)
      (tset tile :l l)
      (tset tile :id mapin.id)
      (tset map :id (+ mapin.id 1))
      (when ?details
        (each [key value (pairs ?details)]
          (tset tile key value))))
    (tset mapin.data l index tile)
    tile)

(fn map.remove [mapin x y l]
  (var index (map.xyl-to-id x y l mapin.width mapin.height))
  (var tile nil)
  (when (. mapin.data l index)
    (set tile (lume.clone (. mapin.data l index))))
  (tset mapin.data l index nil)
    tile)

(fn map.replace [mapin x y l tile ?details]
  (map.remove mapin x y l)
  (map.add-tile mapin x y l tile ?details))

(fn map.get-neighbours [type mapin x y l ?width ?height]
    (let [tiles
          (. mapin.data l)
          neighbour?
          (fn [i j]
            (var max 0)
            (let [xp (if ?width
                         (if  (= (+ x i ) (+ ?width 0)) 0
                              (= (+ x i ) -1) (- ?width 1)
                              (+ x i) )
                         (+ x i))
                  tile (. tiles
                         (map.xyl-to-id xp  (+ y j) l mapin.width mapin.height))]
              (if tile
                  (if (= type (. tile :type))
                      1
                      0)
                  0)))]
      {:right
       (neighbour? 1 0)
       :left (neighbour? -1 0)
       :up (neighbour? 0 -1)
       :down (neighbour? 0 1)
       :up-right (neighbour? 1 -1)
       :up-left (neighbour? -1 -1)
       :down-right (neighbour? 1 1)
       :down-left (neighbour? -1 1)}))

(fn map.update-neighbours [mapin layer]
  (each [key tile (pairs (. mapin.data layer))]
    (local [ttype x y] [tile.type tile.x tile.y])
    (tset mapin :data layer key :index
          (subtile.match  (map.get-neighbours ttype mapin x y layer  mapin.width mapin.height)))))

map
