;; move to lua?
(global white [(/ 193 256) (/ 229 256) (/ 234 256) 1])
(local gamestate (require :lib.gamestate))
(local repl (require :lib.stdio))
(local cargo (require :lib.cargo))

(global profile false)
(global develop false)
(global draw-beam true)

(fn info [obj]
  (each  [key value (pairs obj)] (print key (type value) (if (~= (type value) "table" ) value))))

(global ip info)
;; (global font-ui (assets.fonts.inconsolata 20))

(global wrappedMouseGetPosition
      (fn [self]
        (local (w h flags) (love.window.getMode))
        (local {: fullscreen} flags)
        (local (x y) (love.mouse.getPosition))
        (local params (require :params))
        (local off-x (math.floor  (/ (- w params.screen-width) 2)))
        (if fullscreen
            (values (math.max 0 (math.min params.screen-width (- x off-x))) y)
            (values x y))
        ))

(global cameraGetPosition (fn [self]
      (local params (require :params))
      (local [x y] [(wrappedMouseGetPosition)])
      (local camera self.world.camera)
      ;; (local (w h _) (love.window.getMode))
      ;; (local ox (math.max 0 (math.floor (-  (/ w 2) (/ params.screen-width 2)))))
      ;; (local oy (math.max 0 (math.floor (-  (/ h 2) (/ params.screen-height 2)))))
      (local scale (camera:getScale))
      (local (cx cy) (camera:getVisible))
      [(+ cx (/ x scale)) (+ cy (/ y scale))]))

(math.randomseed (os.time))

(fn start-rain []
  (local state (require :state))
  (local rain (require :prefab-rain))
  (tset state :rain (rain.new)))

(fn love.update [dt]
  (local state (require :state))
  (state.rain:update dt))

(tset _G :web false)

(fn love.load [args uargs]
  (local params (require :params))
  (when (= :web (. args 1))
    (tset _G :web true))
  (love.graphics.setBackgroundColor params.colours.background)
  (love.filesystem.setIdentity "Apocalypse Meow")
  (love.graphics.setDefaultFilter "nearest" "nearest")
  ;; load all assets
  (tset _G :assets (cargo.init
                    {:dir :assets
                     :loaders
                     {:fnl (fn [path] (lume.trim path ".fnl"))
                      :lua (fn [path] (. (lume.split path ".") 1))
                      :json (fn [path]
                              (local json (require :lib.json))
                              (json.decode (love.filesystem.read path)))}
                     :processors
                     {"sounds/"
                      (fn [sound _filename]
                        (sound:setVolume 0.1)
                         sound)
   }
                     }))
  (require :level-management) ;; globals
  (require :handlers)
  (local bgm (if _G.web
                 (love.audio.newSource "assets/audio/Baseline.ogg" :static)
                 (. _G.assets.audio "Baseline")))
  (bgm:setLooping true)
  (bgm:setVolume 0.25)
  (bgm:play)

  (local rainsfx
         (if _G.web
             (love.audio.newSource "assets/audio/rain.ogg" :static)
              _G.assets.audio.rain)
  )
  (rainsfx:setLooping true)
  (rainsfx:setVolume 0.25)
  (rainsfx:play)
  
  (gamestate.registerEvents)
  ;; enter the first game state
  (start-rain)
  (when profile
    (set love.profiler (require "lib.profile"))
    (love.profiler.hookall "Lua")
    (love.profiler.start))  
  (gamestate.switch (require :mode-menu) :wrap)
  (when _G.web    
    (local params (require :params))
    ;; (love.window.setMode 480 350)
    (tset params :scale 2))
  (when (not _G.web) (repl.start))
  )
