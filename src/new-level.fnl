{
  :data {
    :clouds {}
    :for1 {}
    :for2 {}
    :ground {
      -1 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 0
      }
      30 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 0
      }
      59 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 1
      }
      90 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 1
      }
      119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 2
      }
      150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 2
      }
      179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 3
      }
      210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 3
      }
      239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 4
      }
      270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 4
      }
      299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 5
      }
      330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 5
      }
      359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 6
      }
      390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 6
      }
      419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 7
      }
      450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 7
      }
      479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 8
      }
      510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 8
      }
      539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 9
      }
      570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 9
      }
      599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 10
      }
      630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 10
      }
      659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 11
      }
      690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 11
      }
      719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 12
      }
      750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 12
      }
      779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 13
      }
      810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 13
      }
      839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 14
      }
      870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 14
      }
      899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 15
      }
      930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 15
      }
      959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 16
      }
      990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 16
      }
      1019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 17
      }
      1050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 17
      }
      1079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 18
      }
      1110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 18
      }
      1139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 19
      }
      1170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 19
      }
      1199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 20
      }
      1230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 20
      }
      1259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 21
      }
      1290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 21
      }
      1319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 22
      }
      1350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 22
      }
      1379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 23
      }
      1410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 23
      }
      1439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 24
      }
      1470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 24
      }
      1499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 25
      }
      1530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 25
      }
      1559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 26
      }
      1590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 26
      }
      1619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 27
      }
      1650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 27
      }
      1679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 28
      }
      1710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 28
      }
      1739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 29
      }
      1770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 29
      }
      1799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 30
      }
      1830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 30
      }
      1859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 31
      }
      1890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 31
      }
      1919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 32
      }
      1950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 32
      }
      1979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 33
      }
      2010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 33
      }
      2039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 34
      }
      2070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 34
      }
      2099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 35
      }
      2130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 35
      }
      2159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 36
      }
      2190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 36
      }
      2219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 37
      }
      2250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 37
      }
      2279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 38
      }
      2310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 38
      }
      2339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 39
      }
      2370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 39
      }
      2399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 40
      }
      2430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 40
      }
      2459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 41
      }
      2490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 41
      }
      2519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 42
      }
      2550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 42
      }
      2579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 43
      }
      2610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 43
      }
      2639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 44
      }
      2670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 44
      }
      2699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 45
      }
      2730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 45
      }
      2759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 46
      }
      2790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 46
      }
      2819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 47
      }
      2850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 47
      }
      2879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 48
      }
      2910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 48
      }
      2939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 49
      }
      2970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 49
      }
      2999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 50
      }
      3030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 50
      }
      3059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 51
      }
      3090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 51
      }
      3119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 52
      }
      3150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 52
      }
      3179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 53
      }
      3210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 53
      }
      3239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 54
      }
      3270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 54
      }
      3299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 55
      }
      3330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 55
      }
      3359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 56
      }
      3390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 56
      }
      3419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 57
      }
      3450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 57
      }
      3479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 58
      }
      3510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 58
      }
      3539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 59
      }
      3570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 59
      }
      3599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 60
      }
      3630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 60
      }
      3659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 61
      }
      3690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 61
      }
      3719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 62
      }
      3750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 62
      }
      3779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 63
      }
      3810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 63
      }
      3839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 64
      }
      3870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 64
      }
      3899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 65
      }
      3930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 65
      }
      3959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 66
      }
      3990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 66
      }
      4019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 67
      }
      4050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 67
      }
      4079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 68
      }
      4110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 68
      }
      4139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 69
      }
      4170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 69
      }
      4199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 70
      }
      4230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 70
      }
      4259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 71
      }
      4290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 71
      }
      4319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 72
      }
      4350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 72
      }
      4379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 73
      }
      4410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 73
      }
      4439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 74
      }
      4470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 74
      }
      4499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 75
      }
      4530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 75
      }
      4559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 76
      }
      4590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 76
      }
      4619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 77
      }
      4650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 77
      }
      4679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 78
      }
      4710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 78
      }
      4739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 79
      }
      4770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 79
      }
      4799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 80
      }
      4830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 80
      }
      4859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 81
      }
      4890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 81
      }
      4919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 82
      }
      4950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 82
      }
      4979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 83
      }
      5010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 83
      }
      5039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 84
      }
      5070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 84
      }
      5099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 85
      }
      5130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 85
      }
      5159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 86
      }
      5190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 86
      }
      5219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 87
      }
      5250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 87
      }
      5279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 88
      }
      5310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 88
      }
      5339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 89
      }
      5370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 89
      }
      5399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 90
      }
      5430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 90
      }
      5459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 91
      }
      5490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 91
      }
      5519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 92
      }
      5550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 92
      }
      5579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 93
      }
      5610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 93
      }
      5639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 94
      }
      5670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 94
      }
      5699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 95
      }
      5730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 95
      }
      5759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 96
      }
      5790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 96
      }
      5819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 97
      }
      5850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 97
      }
      5879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 98
      }
      5910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 98
      }
      5939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 99
      }
      5970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 99
      }
      5999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 100
      }
      6030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 100
      }
      6059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 101
      }
      6090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 101
      }
      6119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 102
      }
      6150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 102
      }
      6179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 103
      }
      6210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 103
      }
      6239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 104
      }
      6270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 104
      }
      6299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 105
      }
      6330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 105
      }
      6359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 106
      }
      6390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 106
      }
      6419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 107
      }
      6450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 107
      }
      6479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 108
      }
      6510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 108
      }
      6539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 109
      }
      6570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 109
      }
      6599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 110
      }
      6630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 110
      }
      6659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 111
      }
      6690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 111
      }
      6719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 112
      }
      6750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 112
      }
      6779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 113
      }
      6810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 113
      }
      6839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 114
      }
      6870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 114
      }
      6899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 115
      }
      6930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 115
      }
      6959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 116
      }
      6990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 116
      }
      7019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 117
      }
      7050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 117
      }
      7079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 118
      }
      7110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 118
      }
      7139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 119
      }
      7170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 119
      }
      7199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 120
      }
      7230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 120
      }
      7259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 121
      }
      7290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 121
      }
      7319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 122
      }
      7350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 122
      }
      7379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 123
      }
      7410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 123
      }
      7439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 124
      }
      7470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 124
      }
      7499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 125
      }
      7530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 125
      }
      7559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 126
      }
      7590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 126
      }
      7619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 127
      }
      7650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 127
      }
      7679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 128
      }
      7710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 128
      }
      7739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 129
      }
      7770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 129
      }
      7799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 130
      }
      7830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 130
      }
      7859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 131
      }
      7890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 131
      }
      7919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 132
      }
      7950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 132
      }
      7979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 133
      }
      8010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 133
      }
      8039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 134
      }
      8070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 134
      }
      8099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 135
      }
      8130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 135
      }
      8159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 136
      }
      8190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 136
      }
      8219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 137
      }
      8250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 137
      }
      8279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 138
      }
      8310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 138
      }
      8339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 139
      }
      8370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 139
      }
      8399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 140
      }
      8430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 140
      }
      8459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 141
      }
      8490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 141
      }
      8519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 142
      }
      8550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 142
      }
      8579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 143
      }
      8610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 143
      }
      8639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 144
      }
      8670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 144
      }
      8699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 145
      }
      8730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 145
      }
      8759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 146
      }
      8790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 146
      }
      8819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 147
      }
      8850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 147
      }
      8879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 148
      }
      8910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 148
      }
      8939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 149
      }
      8970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 149
      }
      8999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 150
      }
      9030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 150
      }
      9059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 151
      }
      9090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 151
      }
      9119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 152
      }
      9150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 152
      }
      9179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 153
      }
      9210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 153
      }
      9239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 154
      }
      9270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 154
      }
      9299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 155
      }
      9330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 155
      }
      9359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 156
      }
      9390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 156
      }
      9419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 157
      }
      9450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 157
      }
      9479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 158
      }
      9510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 158
      }
      9539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 159
      }
      9570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 159
      }
      9599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 160
      }
      9630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 160
      }
      9659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 161
      }
      9690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 161
      }
      9719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 162
      }
      9750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 162
      }
      9779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 163
      }
      9810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 163
      }
      9839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 164
      }
      9870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 164
      }
      9899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 165
      }
      9930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 165
      }
      9959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 166
      }
      9990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 166
      }
      10019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 167
      }
      10050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 167
      }
      10079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 168
      }
      10110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 168
      }
      10139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 169
      }
      10170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 169
      }
      10199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 170
      }
      10230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 170
      }
      10259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 171
      }
      10290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 171
      }
      10319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 172
      }
      10350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 172
      }
      10379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 173
      }
      10410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 173
      }
      10439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 174
      }
      10470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 174
      }
      10499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 175
      }
      10530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 175
      }
      10559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 176
      }
      10590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 176
      }
      10619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 177
      }
      10650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 177
      }
      10679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 178
      }
      10710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 178
      }
      10739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 179
      }
      10770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 179
      }
      10799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 180
      }
      10830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 180
      }
      10859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 181
      }
      10890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 181
      }
      10919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 182
      }
      10950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 182
      }
      10979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 183
      }
      11010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 183
      }
      11039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 184
      }
      11070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 184
      }
      11099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 185
      }
      11130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 185
      }
      11159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 186
      }
      11190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 186
      }
      11219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 187
      }
      11250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 187
      }
      11279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 188
      }
      11310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 188
      }
      11339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 189
      }
      11370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 189
      }
      11399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 190
      }
      11430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 190
      }
      11459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 191
      }
      11490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 191
      }
      11519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 192
      }
      11550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 192
      }
      11579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 193
      }
      11610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 193
      }
      11639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 194
      }
      11670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 194
      }
      11699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 195
      }
      11730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 195
      }
      11759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 196
      }
      11790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 196
      }
      11819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 197
      }
      11850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 197
      }
      11879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 198
      }
      11910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 198
      }
      11939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 199
      }
      11970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 199
      }
      11999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 200
      }
      12030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 200
      }
      12059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 201
      }
      12090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 201
      }
      12119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 202
      }
      12150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 202
      }
      12179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 203
      }
      12210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 203
      }
      12239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 204
      }
      12270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 204
      }
      12299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 205
      }
      12330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 205
      }
      12359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 206
      }
      12390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 206
      }
      12419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 207
      }
      12450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 207
      }
      12479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 208
      }
      12510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 208
      }
      12539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 209
      }
      12570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 209
      }
      12599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 210
      }
      12630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 210
      }
      12659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 211
      }
      12690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 211
      }
      12719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 212
      }
      12750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 212
      }
      12779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 213
      }
      12810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 213
      }
      12839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 214
      }
      12870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 214
      }
      12899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 215
      }
      12930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 215
      }
      12959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 216
      }
      12990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 216
      }
      13019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 217
      }
      13050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 217
      }
      13079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 218
      }
      13110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 218
      }
      13139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 219
      }
      13170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 219
      }
      13199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 220
      }
      13230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 220
      }
      13259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 221
      }
      13290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 221
      }
      13319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 222
      }
      13350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 222
      }
      13379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 223
      }
      13410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 223
      }
      13439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 224
      }
      13470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 224
      }
      13499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 225
      }
      13530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 225
      }
      13559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 226
      }
      13590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 226
      }
      13619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 227
      }
      13650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 227
      }
      13679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 228
      }
      13710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 228
      }
      13739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 229
      }
      13770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 229
      }
      13799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 230
      }
      13830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 230
      }
      13859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 231
      }
      13890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 231
      }
      13919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 232
      }
      13950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 232
      }
      13979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 233
      }
      14010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 233
      }
      14039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 234
      }
      14070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 234
      }
      14099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 235
      }
      14130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 235
      }
      14159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 236
      }
      14190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 236
      }
      14219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 237
      }
      14250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 237
      }
      14279 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 238
      }
      14280 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 238
      }
      14281 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 238
      }
      14282 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 238
      }
      14283 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 238
      }
      14284 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 238
      }
      14285 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 238
      }
      14286 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 238
      }
      14287 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 238
      }
      14288 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 238
      }
      14289 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 238
      }
      14290 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 238
      }
      14291 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 238
      }
      14292 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 238
      }
      14293 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 238
      }
      14294 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 238
      }
      14295 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 238
      }
      14296 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 238
      }
      14297 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 238
      }
      14298 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 238
      }
      14299 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 238
      }
      14300 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 238
      }
      14301 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 238
      }
      14302 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 238
      }
      14303 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 238
      }
      14304 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 238
      }
      14305 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 238
      }
      14306 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 238
      }
      14307 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 238
      }
      14308 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 238
      }
      14309 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 238
      }
      14310 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 238
      }
      14339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 239
      }
      14370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 239
      }
      14399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 240
      }
      14430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 240
      }
      14459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 241
      }
      14490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 241
      }
      14519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 242
      }
      14550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 242
      }
      14579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 243
      }
      14610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 243
      }
      14639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 244
      }
      14670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 244
      }
      14699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 245
      }
      14730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 245
      }
      14759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 246
      }
      14790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 246
      }
      14819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 247
      }
      14850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 247
      }
      14879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 248
      }
      14910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 248
      }
      14939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 249
      }
      14970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 249
      }
      14999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 250
      }
      15030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 250
      }
      15059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 251
      }
      15090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 251
      }
      15119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 252
      }
      15150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 252
      }
      15179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 253
      }
      15210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 253
      }
      15239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 254
      }
      15270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 254
      }
      15299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 255
      }
      15330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 255
      }
      15359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 256
      }
      15390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 256
      }
      15419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 257
      }
      15450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 257
      }
      15479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 258
      }
      15510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 258
      }
      15539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 259
      }
      15570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 259
      }
      15599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 260
      }
      15630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 260
      }
      15659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 261
      }
      15690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 261
      }
      15719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 262
      }
      15750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 262
      }
      15779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 263
      }
      15810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 263
      }
      15839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 264
      }
      15870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 264
      }
      15899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 265
      }
      15930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 265
      }
      15959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 266
      }
      15990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 266
      }
      16019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 267
      }
      16050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 267
      }
      16079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 268
      }
      16110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 268
      }
      16139 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 269
      }
      16170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 269
      }
      16199 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 270
      }
      16230 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 270
      }
    }
    :objs {
      14169 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 128
        :l "objs"
        :library "sprite"
        :sprite "end-gate"
        :type "gate"
        :w 16
        :x 9
        :y 236
      }
      14175 {
        :flipped false
        :h 32
        :id 0
        :image-h 224
        :image-w 704
        :l "objs"
        :library "sprite"
        :sprite "samuel"
        :type "player"
        :w 32
        :x 15
        :y 236
      }
    }
    :sun {}
  }
  :height 34
  :id 0
  :tile-size 16
  :width 60
}