(local gamera (require :lib.gamera))
(local params (require :params))


(fn [player]
  (local cam (gamera.new 0 0 params.map-width params.map-height))
  (tset cam :camera true)
  (tset cam :active player.active)
  (tset cam :player player)
  (tset cam :speed params.speed)
  (local (w h flags) (love.window.getMode))
  (local {: fullscreen} flags)
  (if fullscreen
      (cam:setWindow (math.floor (/ (- w  params.screen-width) 2)) 0
                     params.screen-width h)
      (cam:setWindow 0 0 params.screen-width params.screen-height))
  (cam:setScale params.scale)
  (cam:setPosition 0 params.map-height)
  cam)
