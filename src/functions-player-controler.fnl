(local player-controler {})

(local tiny (require :lib.tiny))

(local params (require :params))

(tset player-controler :filter (tiny.requireAll "player" "active" (tiny.rejectAny"camera")))

(fn player-controler.process [self player-entity args]
  (match (type args)
        :table ((. player-controler self.world.callback) self player-entity
                (unpack args))
        _ ((. player-controler self.world.callback) self player-entity args)))

(fn check-mouse-ray [self player]
  (local bump (require :lib.bump))
  (local [mxp myp] (cameraGetPosition self))
  (local entity (. (require :state) :entity :player))
  (local [w2 h2] [(/ entity.size.w 2) (/ entity.size.h 2)])
  (local [px py] [(+ entity.pos.x w2) (+ entity.pos.y h2)])
  ;; (pp [mx mxp px (- px mxp)])
  (fn collision-filter [item]
    (match item.type
      :floor true
      _ false))
  (fn distance-sort-fun  [{:x x1 :y y1} {:x x2 :y y2}]
    (> (lume.distance px py x1 y1 :squared)
       (lume.distance px py x2 y2 :squared)))
  ;;local items, len = world:queryPoint(x,y, filter)
  ;; local items, len = world:querySegment(x1,y1,x2,y2,filter)
  (local world player.collider.world)

  (local (point-items point-len) (world:queryPoint mxp myp collision-filter))
  (local (segment-items segment-len) (world:querySegment px py mxp myp collision-filter))
  ;; (lume.sort segment-items distance-sort-fun)
  ;;(tset player :hook-block point-items)
  ;; (db (lume.map segment-items (fn [{: x : y}] (lume.distance px py x y :squared))) )
  (when (~= :hook player.state-machine.current-state)
    (if (> segment-len 0)
      ;; (do (db "setting player hook block")
        (tset player :hook-block (. segment-items 1))
        (tset player :hook-block nil)
      ))
  )

(fn player-controler.c-update [self player-entity dt]
  (local state (require :player-state))
  (local animation (require :player-animation))
  (local movement (require :player-movement))
  (check-mouse-ray self player-entity)
  (let [[current-state [cols len]]
        ;; Identify colliding hit box
        (match player-entity.state-machine.current-state
          :fall [:fall (movement.movement-fall player-entity dt)]
          :jump [:jump (movement.movement-jump player-entity dt)]
          :walk [:walk (movement.movement-walk player-entity dt)]
          :hook [:hook (movement.movement-hook player-entity dt)]
          ;; :idle [:nothing [{} 0]]
          _ [:nothing [{} 0]]
          )]
    (local previous-state (state player-entity current-state dt))
    (animation self player-entity player-entity.state-machine.current-state previous-state dt)
    )
  )

(fn player-controler.mousepressed [self player-entity x y button])

(fn player-controler.mousereleased [self player-entity x y button])

{ : player-controler}
