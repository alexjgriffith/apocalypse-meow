(local tiny (require :lib.tiny))

(local cat-system {})

(tset cat-system :filter (tiny.requireAll "active" "sprite" "cat"))

(fn cat-system.process [self entity args]
    (match (type args)
        :table ((. cat-system self.world.callback) self entity
                (unpack args))
        _ ((. cat-system self.world.callback) self entity args)))

(local distance-fast true)

(fn check-stand-distance [cat-entity]
  (if distance-fast
      (do
          (local player (. (require :state) :entity :player))
        (let [{:x x1 :y y1 :w w1 :h h1} cat-entity.stand-collider
              {:x x2 :y y2} player.pos
              {:w w2 :h h2} player.size
              ]
          (rect_isIntersecting x1 y1 w1 h1 x2 y2 w2 h2)))
      (let [{:x x :y y :w w :h h} cat-entity.stand-collider]
        (local (items len) (cat-entity.bump-reference.world:queryRect x y w h
                                                                      (fn [other]
                                                                        (match other.type
                                                                          :player true
                                                                          _ false)))
               )
        (> len 0))
  ))

(fn check-save-distance [cat-entity]
  (if distance-fast
      (do (local player (. (require :state) :entity :player))
        (let [{:x x1 :y y1 :w w1 :h h1} cat-entity.save-collider
              {:x x2 :y y2} player.pos
              {:w w2 :h h2} player.size]
          (rect_isIntersecting x1 y1 w1 h1 x2 y2 w2 h2)))
      (let [{: x : y : w : h} cat-entity.save-collider]
        (local (items len) (cat-entity.bump-reference.world:queryRect x y w h
                                                                      (fn [other]
                                                                        (match other.type
                                                                          :player true
                                                                          _ false))))
        (> len 0)))
  )

(fn check-saved [cat-entity]
  cat-entity.saved)

(fn standing [cat-entity]
  (or (= :stand cat-entity.state) (= :active cat-entity.state)))

(fn not-standing [cat-entity]
  (or (= :sit cat-entity.state) (= :wait cat-entity.state)))

(fn cat-system.c-update [self cat-entity dt]
  ;; check stand distance
  (var next-state cat-entity.state)
  (local stand-distance-t (check-stand-distance cat-entity))
  (when (and
         stand-distance-t
         (not-standing cat-entity))
    (set next-state :stand)
    (love.event.push :cat-near))

  (when (and
         (not stand-distance-t)
         (standing cat-entity))
    (set next-state :sit))

  ;; check water level
  (when (or (check-save-distance cat-entity) (check-saved cat-entity))
    (when (not cat-entity.saved)
      (cat-entity.bump-reference.world:remove cat-entity)
      (love.event.push :cat-saved cat-entity.name))
    (tset cat-entity :saved true)
    (set next-state :float))

  (when cat-entity.dead
      (set next-state :drown))
  ;; reset animation if there is a change in state
  (when (and next-state (~= next-state cat-entity.state))
    (tset cat-entity :animations next-state :position 1)
    (tset cat-entity :animations next-state :timer 0))

  (when next-state
    (tset cat-entity :state next-state))
  ;; check water level
  ;; check saved
  ;;
  (when cat-entity.saved
    (set cat-entity.mod-colour [1 0 0 1])
    (set cat-entity.pos.y (- cat-entity.pos.y 1)))

  (when cat-entity.dead
    (when (cat-entity.bump-reference.world:hasItem cat-entity)
        (cat-entity.bump-reference.world:remove cat-entity))
    (set cat-entity.mod-colour [1 0 0 1])
    (set cat-entity.pos.y (+ cat-entity.pos.y 2)))

  (: (. cat-entity.animations cat-entity.state) :update dt)
  )

{ : cat-system}
