(local map-editor (require :map-editor))

(fn [file-name]
  (map-editor file-name
              assets.sprites
              "tiles-and-objects-object"
              (. (require :neon-levels) :tile-data)
              (. (require :neon-levels) :states)))
