(local menu {})

(local gamestate (require :lib.gamestate))
(local tiny (require :lib.tiny))
(local buttons (require :lib.buttons))
(local params (require :params))


(local game-text
       "You've done it, you've saved our feline friends!

Rest assured they are all happy in cat heaven eating cat nip and enjoying their after-nine-lives to the upmost.

Now you can go in peace.")


(local elements
       [{:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Continue" }
        ;; {:type :small-text :y 200 :oy -10 :ox 0 :w 900 :h 70 :text game-text
        ;;}
       ])

(local element-font
       {:title  ((. assets.fonts "operational amplifier") 70)
        :button  ((. assets.fonts "operational amplifier") 40)
        ;; :small-text  ((. assets.fonts "inconsolata") 20)
        })

(local element-click
       {"Continue"
          (fn []
            (assets.sounds.scratch:play)
            (menu.game-text-reading:reset)
            (gamestate.switch (require :mode-game-over) )
            )
          })

(local element-hover {:button (fn [element x y] :nothing)})


(local ui (buttons elements params element-click element-hover element-font))

(local loader (require :lib.loader))
(local ghost (loader "ghost" 16))

(tset  menu :game-text-reading ((require :beep-speak-box)
                          280 100
                          400 180
                          game-text
                          (assets.fonts.inconsolata 20)
                          "assets/sounds/F# Beep Scale.wav"
                          (* 4 (/ 60.0 280))
                         true))

(menu.game-text-reading:start)

(var talking true)

(fn menu.draw [self]
  (local [cx cy] (ui:c-offset))
  (love.graphics.push)
  (love.graphics.scale 4.0)
  (love.graphics.setColor 1 1 1 1)
  (ghost.animations.talk:draw ghost.image (+ 110 (math.floor (/ cx 4))) (+ 90 (math.floor (/ cy 4))))
  (love.graphics.pop)
  (love.graphics.setFont (assets.fonts.inconsolata 20))
  (love.graphics.setColor 1 1 1 1)
  (menu.game-text-reading:draw cx cy)
  (ui:draw))

(fn menu.update [self dt]
  ;; (set game-text-reading.font (assets.fonts.inconsolata 20))
  (match (menu.game-text-reading:update dt)
    :end (set talking false))
  (if talking
      (ghost.animations.talk:update dt)
      (tset ghost :animations :talk :index 1))
  (ui:update dt))

(fn menu.enter [menu]
  (menu.game-text-reading:reset)
  (menu.game-text-reading:start)
  )

(fn menu.leave [_]
  ;; (pp "menu exit")
  ;; (set menu.game-text-reading.silet true)
  )

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

(fn menu.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "r" (menu.game-text-reading:reset)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

menu
