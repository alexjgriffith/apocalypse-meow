(local bump (require "lib.bump"))
(local bump-plugin (require "bump-plugin"))

(fn [collidables level]
  (local bump-world {:collidables collidables
                     :world (bump.newWorld 32)
                     :name "bump-world"
                     :render true
                     :bump-world true})
  (bump-plugin.add-layer bump-world.world collidables level :ground 1)
  bump-world)
