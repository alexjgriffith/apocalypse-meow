;; from stl
;; local t = {
;;         name       = object.name,
;;         type       = object.type,
;;         x          = instance.x + map.offsetx + object.x,
;;         y          = instance.y + map.offsety + object.y,
;;         width      = object.width,
;;         height     = object.height,
;;         layer      = instance.layer,
;;         properties = object.properties
;;         }
;; world:add(t, t.x, t.y, t.width, t.height)

;;(local l1 (require "level1"))

(fn add-tile [world collidables id tile tile-size scale? offset-x?]
  (let [offset-x (or offset-x? 0)
        scale (or scale? 1)
          t {:name id
              :type tile.type
              :w (* scale (* (or tile.w 1) tile-size))
              :h (* scale (* (or tile.h 1) tile-size))
              :x (* scale (+ (* tile.x tile-size) offset-x))
              :y (* scale (* tile.y tile-size))
              :l tile.l
              :id tile.id
              :library tile.library
              }]
      (: world :add t t.x t.y t.w t.h)
      (table.insert collidables t)))



(fn add-subtile [world collidables id tile tile-size scale? offset-x?]
  ;; key 1
  (local [tr tl br bl] tile.index)
  (each [key subtile (pairs {: tr : tl : br : bl})]
    (let [surfaces {1 true 2 true 3 true 4 true
                    5 true 6 true 7 true 8 true
                    11 true 12 true 13 true} ;; not the bottom or middle
          offset-x (or offset-x? 0)
          scale (or scale? 1)
          ox (match key :tr 0 :tl 0.5 :br 0 :bl 0.5)
          oy (match key :tr 0 :tl 0 :br 0.5 :bl 0.5)
          t {:name (.. id "-" key)
             :type tile.type
             :w (* scale (/ (* (or tile.w 1) tile-size) 2))
             :h (* scale (/ (* (or tile.h 1) tile-size) 2))
             :x (* scale (+ (* (+ tile.x ox) tile-size) offset-x))
             :y (* scale (* (+ tile.y oy) tile-size))
             :l tile.l
             :id id
             :library tile.library
             }]
      (when (. surfaces subtile)
        (: world :add t t.x t.y t.w t.h)
        (table.insert collidables t)))))

(fn draw  [collidables world ?tx ?ty ?sx ?sy]
    (love.graphics.push)
    (love.graphics.translate (math.floor (or ?tx 0))
                             (math.floor (or ?ty 0)))
    (love.graphics.scale  (or ?sx 1) (or ?sy (or ?sx 1)))
    (each [_ collidable (pairs (: world :getItems))]
          (love.graphics.rectangle
           "line"
           (: world :getRect collidable)))
    (love.graphics.pop))

(fn add-layer [world collidables map layer scale offset-x]
  (each [id tile (pairs (. map :data layer))]
    ((match tile.library
       :tile add-tile
       :subtile add-subtile)
     world collidables id tile map.tile-size offset-x)))

{: draw : add-layer : add-tile : add-subtile}
