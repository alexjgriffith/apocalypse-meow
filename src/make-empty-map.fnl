(local map-editor (require :map-editor))
(local params (require :params))


(fn [header footer]
  (local empty-map
         (map-editor nil
                     assets.sprites
                     "tiles-and-objects-object"
                     (. (require :neon-levels) :tile-data)
                     (. (require :neon-levels) :states)))
  (local tile-width 16)
  (local height (/ params.map-height 16))
  (local width (/ params.map-width 16))
  (local start-column -1)
  (local mid-column (math.floor (/ width 2)))
  (local end-column (+ width 0)) ;; screen width +1
  (local start-row (- height 32)) ;; just over two screen height
  (pp [height width start-column mid-column end-column start-row])
  (for [i 0 height]
    (empty-map:add-tile :floor start-column i :non-interactive)
    (empty-map:add-tile :floor end-column i :non-interactive))
  (for [j 0 width]
    (empty-map:add-tile :floor j start-row :non-interactive))

  (tset empty-map :header header)
  (tset empty-map :footer footer)
  (empty-map:add-tile :player  mid-column (- start-row 2))
  empty-map)
