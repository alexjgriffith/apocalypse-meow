(local gamera (require :lib.gamera))
(local params (require :params))


(fn [editor]
  (local cam (gamera.new 0 0 params.map-width params.map-height))
  (tset cam :camera true)
  (tset cam :active editor.active)
  (tset cam :editor editor)
  (tset cam :speed params.speed)
  (cam:setScale params.scale)
  (cam:setPosition 0 params.map-height)
  cam
  )
