(local menu {})

(local gamestate (require :lib.gamestate))
(local buttons (require :lib.buttons))
(local params (require :params))

(local element-font
       {:title  ((. assets.fonts "operational amplifier") 70)
        :button  ((. assets.fonts "operational amplifier") 40)
        })

(local element-click
       {"Make Fullscreen"
        (fn []
          (toggle-fullscreen)
          (gamestate.switch (require "mode-options")))

        "Make Windowed"
        (fn []
          (toggle-fullscreen)
          (gamestate.switch (require "mode-options")))

        "Lock Mouse In Game"
        (fn []
          (toggle-mouselock)
          (gamestate.switch (require "mode-options")))

        "Unlock Mouse In Game"
        (fn []
          (toggle-mouselock)
          (gamestate.switch (require "mode-options")))

        "Show Mouse"
        (fn []
          (toggle-mousevisible)
          (gamestate.switch (require "mode-options")))

        "Hide Mouse"
        (fn []
          (toggle-mousevisible)
          (gamestate.switch (require "mode-options")))

        "Mute"
        (fn []
          (toggle-sound)
          (gamestate.switch (require "mode-options")))

        "Play Music"
        (fn []
          (toggle-sound)
          (gamestate.switch (require "mode-options")))

        "Credits"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-credits")))

        "Controls"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-controls")))

        "Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu") :credits))
        }

       )

(local element-hover {:button (fn [element x y] :nothing)})


(var ui nil)

(fn menu.enter [self]
  (local elements
         [{:type :title :y 10 :w 400 :h 60 :text "OPTIONS"}
          (if (is-fullscreen)
              {:type :button :y 100 :oy -10 :ox 0 :w 400 :h 70 :text "Make Windowed" }
              {:type :button :y 100 :oy -10 :ox 0 :w 400 :h 70 :text "Make Fullscreen" }
              )
          (if (is-mouselock)
              {:type :button :y 200 :oy -10 :ox 0 :w 400 :h 70 :text "Unlock Mouse In Game" }
              {:type :button :y 200 :oy -10 :ox 0 :w 400 :h 70 :text "Lock Mouse In Game" }
              )
          (if (is-mute)
              {:type :button :y 400 :oy -10 :ox 0 :w 400 :h 70 :text "Play Music"}
              {:type :button :y 400 :oy -10 :ox 0 :w 400 :h 70 :text "Mute"}
              )
          ;; (if (is-mousevisible)
          ;;     {:type :button :y 300 :oy -10 :ox 0 :w 400 :h 70 :text "Hide Mouse"}
          ;;     {:type :button :y 300 :oy -10 :ox 0 :w 400 :h 70 :text "Show Mouse"}
          ;;     )
          {:type :button :y 300 :oy -10 :ox 0 :w 400 :h 70 :text "Controls"}
          {:type :button :y 500 :oy -10 :ox 0 :w 400 :h 70 :text "Credits"}
          {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Back"}
        ;; {:type :small-text :y 200 :oy -10 :ox 0 :w 900 :h 70 :text game-text
        ;;}
        ])
  (set ui (buttons elements params element-click element-hover element-font))
)

(fn menu.draw [self]
  (ui:draw))

(fn menu.update [self dt]
  (ui:update dt))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

(fn menu.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (screenshot)
    "f10" (toggle-fullscreen)))

menu
