(local timer (require :lib.timer))
(local cargo (require :lib.cargo))

(local sprite-watcher {})

(fn default-callback [changed-files]
  (each [_ file (ipairs changed-files)]
    ;;(tset  assets.sprites file (love.graphics.newImage (.. "assets/sprites/" file)))
  (tset assets :sprites (cargo.init "assets/sprites"))
  ))

(fn sprite-watcher.init [end? callback?]
  ;; (love.filesystem.getSource)
  (local directory  "/assets/sprites/")
  (local files (love.filesystem.getDirectoryItems directory) )
  (local ret {:sprites {}
              :directory directory
              :files files
              :callback (or callback? default-callback)
              :timer (timer (or end? 1))})
  (each [_ file (ipairs files)]
    (tset ret.sprites file (love.filesystem.getInfo (.. directory  file))))
  ret)

(fn sprite-watcher.changes [self]
  (local files (love.filesystem.getDirectoryItems self.directory))
  (tset self :files files)
  (var changes false)
  (local changed-files [])
  (each [_ file (ipairs files)]
    (let [file-info (love.filesystem.getInfo (.. self.directory  file))]
      (when (~= file-info.modtime (. (. self.sprites file) :modtime))
        (set changes true)
        (table.insert changed-files file)
        (tset self.sprites file file-info))))
  (values changes changed-files))

(fn sprite-watcher.update [self dt]
  (when (self.timer:update dt)
    (let [(change changed-files) (self:changes)]
      (when change
        (self.callback changed-files)))
    (self.timer:reset)))

(fn sprite-watcher.print [self]
  (pp self.sprites))

(local sprite-watcher-mt
       {:__index sprite-watcher
        :print sprite-watcher.print
        :update sprite-watcher.update
        :changes sprite-watcher.changes})
(fn []
  (setmetatable (sprite-watcher.init) sprite-watcher-mt))
