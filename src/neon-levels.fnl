(local neon-tile-data
       [{:name :floor  :tile-fun :square10  :x1 1 :y1 1 :x2 3 :y2 1}
        ;; {:name :light  :tile-fun :sprite  :x 6 :y 1 :w 2 :h 4}
        ;; {:name :trap  :tile-fun :sprite  :x 8 :y 1 :w 4 :h 2}
        ;; {:name :outline  :tile-fun :sprite  :x 3 :y 4 :w 2 :h 4}
        ;; {:name :outline-right  :tile-fun :sprite  :x 1 :y 4 :w 2 :h 4}
        {:name :cat  :tile-fun :sprite  :x 0 :y 0 :w 32 :h 32 :sprite :cat-outline
         :image-w (* 26 32) :image-h 32}
        {:name :gate  :tile-fun :sprite  :x 0 :y 0 :w 16 :h 32 :sprite :end-gate
         :image-w (* 8 16) :image-h 32}
        {:name :player  :tile-fun :sprite  :x 0 :y 0 :w 32 :h 32 :sprite :Shadow
         :image-w (* 22 32) :image-h (* 7 32)}])

(local neon-states
       {:night ;; need to fix these. object update is passing references
        {:activities
         [{:stat "hunger" :activity :eat :base 10 :bias 1}
          {:stat "hunger" :activity  :meaw :base 1 :bias 1}]
         :health 1
         :next-state :food-empty}})

{:states neon-states :tile-data neon-tile-data}
