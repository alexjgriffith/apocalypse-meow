(local buttons (require :lib.buttons))
(local tiny (require :lib.tiny))

(local update-ui {})

(tset update-ui :filter (tiny.requireAll "ui" "active"))

(fn update-ui.process [self ui dt]
  (local player ui.player-reference)
  (tset ui :message
         (string.format "x: %s y: %s state: %s\nspeed-x: %s speed-y: %s
count: %s min-time: %s max-time: %s"
                 (math.floor player.pos.x)
                 (math.floor player.pos.y)
                 player.state-machine.current-state
                 (math.floor player.speed.x)
                 (math.floor player.speed.y)
                 player.controler.jump.count
                 (/ (math.floor (* 100 player.controler.jump.timer.time)) 100)
                 (/ (math.floor (* 100 player.controler.jump.timer.end)) 100)))
;;   (local message-animation
;;          (string.format "State: %s
;; Current State: %s %s
;; Previous State: %s %s
;; Speed X: %s %s
;; Spped Y: %s %s
;; Action: %s %s"
;;                         player.animation-state
;;                         player.animation.requirements.state
;;                         player.animation.checks.state
;;                         player.animation.requirements.previous-state
;;                         player.animation.checks.previous-state
;;                         player.animation.requirements.speed-x
;;                         player.animation.checks.speed-x
;;                         player.animation.requirements.speed-y
;;                         player.animation.checks.speed-y2
;;                         player.animation.requirements.action
;;                         player.animation.checks.action))

  (local state (require :state))
  (local level (. state.levels state.level))
  (local seconds-to-time (require :time))
  (local time-string (seconds-to-time level.time))
  ;; (tset ui :hud (string.format "%s\n%s\nCats Saved: %s\nCats Left:  %s"
  ;;                              state.level
  ;;                              time-string
  ;;                              level.saved
  ;;                              (- level.total-cats level.saved level.lost)))

  (tset ui :hud (.. state.level  " Cats: " level.saved "/" level.total-cats))

  (tset ui :save-declaration (if (or (= "" level.most-recently-saved)
                                       (ui.save-declaration-timer:update dt))
                              ""
                              (.. "You've saved " level.most-recently-saved "!")))

  )

{ : update-ui}
