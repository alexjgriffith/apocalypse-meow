{
  :data {
    :clouds {}
    :for1 {}
    :for2 {}
    :ground {
      -1 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 0
      }
      30 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 0
      }
      59 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 1
      }
      90 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 1
      }
      119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 2
      }
      150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 2
      }
      179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 3
      }
      210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 3
      }
      239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 4
      }
      270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 4
      }
      299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 5
      }
      330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 5
      }
      359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 6
      }
      390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 6
      }
      419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 7
      }
      450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 7
      }
      479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 8
      }
      510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 8
      }
      539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 9
      }
      570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 9
      }
      599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 10
      }
      630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 10
      }
      659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 11
      }
      690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 11
      }
      719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 12
      }
      750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 12
      }
      779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 13
      }
      810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 13
      }
      839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 14
      }
      870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 14
      }
      899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 15
      }
      930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 15
      }
      959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 16
      }
      990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 16
      }
      1019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 17
      }
      1050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 17
      }
      1079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 18
      }
      1110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 18
      }
      1139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 19
      }
      1170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 19
      }
      1199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 20
      }
      1230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 20
      }
      1259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 21
      }
      1290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 21
      }
      1319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 22
      }
      1350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 22
      }
      1379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 23
      }
      1410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 23
      }
      1439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 24
      }
      1470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 24
      }
      1499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 25
      }
      1530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 25
      }
      1559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 26
      }
      1590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 26
      }
      1619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 27
      }
      1650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 27
      }
      1679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 28
      }
      1710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 28
      }
      1739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 29
      }
      1770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 29
      }
      1799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 30
      }
      1830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 30
      }
      1859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 31
      }
      1890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 31
      }
      1919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 32
      }
      1950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 32
      }
      1979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 33
      }
      2010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 33
      }
      2039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 34
      }
      2049 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 34
      }
      2050 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 34
      }
      2051 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 34
      }
      2052 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 34
      }
      2053 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 34
      }
      2070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 34
      }
      2099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 35
      }
      2130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 35
      }
      2159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 36
      }
      2190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 36
      }
      2219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 37
      }
      2250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 37
      }
      2279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 38
      }
      2310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 38
      }
      2339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 39
      }
      2370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 39
      }
      2399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 40
      }
      2430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 40
      }
      2459 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 41
      }
      2460 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 41
      }
      2461 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 41
      }
      2462 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 41
      }
      2463 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 41
      }
      2464 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 41
      }
      2465 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 41
      }
      2466 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 41
      }
      2467 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 41
      }
      2468 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 41
      }
      2469 {
        :id 0
        :index [8 8 3 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 41
      }
      2470 {
        :id 0
        :index [8 11 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 41
      }
      2477 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 41
      }
      2478 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 41
      }
      2479 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 41
      }
      2480 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 41
      }
      2481 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 41
      }
      2482 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 41
      }
      2483 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 41
      }
      2484 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 41
      }
      2485 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 41
      }
      2486 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 41
      }
      2487 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 41
      }
      2488 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 41
      }
      2489 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 41
      }
      2490 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 41
      }
      2519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 42
      }
      2529 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 42
      }
      2530 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 42
      }
      2550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 42
      }
      2579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 43
      }
      2589 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 43
      }
      2590 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 43
      }
      2610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 43
      }
      2639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 44
      }
      2649 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 44
      }
      2650 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 44
      }
      2670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 44
      }
      2699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 45
      }
      2709 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 45
      }
      2710 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 45
      }
      2716 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 45
      }
      2717 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 45
      }
      2718 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 45
      }
      2719 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 45
      }
      2720 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 45
      }
      2730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 45
      }
      2759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 46
      }
      2769 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 46
      }
      2770 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 46
      }
      2790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 46
      }
      2819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 47
      }
      2829 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 47
      }
      2830 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 47
      }
      2850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 47
      }
      2879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 48
      }
      2884 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 48
      }
      2885 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 48
      }
      2886 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 48
      }
      2887 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 48
      }
      2888 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 48
      }
      2889 {
        :id 0
        :index [4 9 3 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 48
      }
      2890 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 48
      }
      2910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 48
      }
      2939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 49
      }
      2949 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 49
      }
      2950 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 49
      }
      2962 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 49
      }
      2963 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 49
      }
      2964 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 49
      }
      2965 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 49
      }
      2966 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 49
      }
      2970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 49
      }
      2999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 50
      }
      3009 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 50
      }
      3010 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 50
      }
      3030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 50
      }
      3059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 51
      }
      3069 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 51
      }
      3070 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 51
      }
      3090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 51
      }
      3119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 52
      }
      3129 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 52
      }
      3130 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 52
      }
      3150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 52
      }
      3179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 53
      }
      3189 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 53
      }
      3190 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 53
      }
      3210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 53
      }
      3239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 54
      }
      3249 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 54
      }
      3250 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 54
      }
      3270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 54
      }
      3299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 55
      }
      3309 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 55
      }
      3310 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 55
      }
      3330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 55
      }
      3359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 56
      }
      3369 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 56
      }
      3370 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 56
      }
      3376 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 56
      }
      3377 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 56
      }
      3378 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 56
      }
      3379 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 56
      }
      3380 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 56
      }
      3381 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 56
      }
      3382 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 56
      }
      3390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 56
      }
      3419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 57
      }
      3425 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 57
      }
      3429 {
        :id 0
        :index [6 9 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 57
      }
      3430 {
        :id 0
        :index [9 12 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 57
      }
      3450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 57
      }
      3479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 58
      }
      3485 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 58
      }
      3510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 58
      }
      3539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 59
      }
      3545 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 59
      }
      3570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 59
      }
      3599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 60
      }
      3605 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 60
      }
      3630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 60
      }
      3659 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 61
      }
      3660 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 61
      }
      3661 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 61
      }
      3662 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 61
      }
      3663 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 61
      }
      3664 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 61
      }
      3665 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 61
      }
      3673 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 61
      }
      3674 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 61
      }
      3675 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 61
      }
      3676 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 61
      }
      3677 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 61
      }
      3678 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 61
      }
      3679 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 61
      }
      3680 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 61
      }
      3681 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 61
      }
      3682 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 61
      }
      3683 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 61
      }
      3684 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 61
      }
      3685 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 61
      }
      3686 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 61
      }
      3687 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 61
      }
      3688 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 61
      }
      3689 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 61
      }
      3690 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 61
      }
      3719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 62
      }
      3725 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 62
      }
      3750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 62
      }
      3779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 63
      }
      3785 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 63
      }
      3810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 63
      }
      3839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 64
      }
      3845 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 64
      }
      3870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 64
      }
      3899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 65
      }
      3905 {
        :id 0
        :index [6 2 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 65
      }
      3906 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 65
      }
      3907 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 65
      }
      3908 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 65
      }
      3909 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 65
      }
      3910 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 65
      }
      3930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 65
      }
      3959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 66
      }
      3990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 66
      }
      4019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 67
      }
      4050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 67
      }
      4079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 68
      }
      4110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 68
      }
      4139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 69
      }
      4170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 69
      }
      4199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 70
      }
      4205 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 70
      }
      4230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 70
      }
      4259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 71
      }
      4265 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 71
      }
      4290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 71
      }
      4319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 72
      }
      4325 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 72
      }
      4350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 72
      }
      4379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 73
      }
      4385 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 73
      }
      4386 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 73
      }
      4387 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 73
      }
      4388 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 73
      }
      4389 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 73
      }
      4390 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 73
      }
      4391 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 73
      }
      4392 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 73
      }
      4393 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 73
      }
      4394 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 73
      }
      4395 {
        :id 0
        :index [8 11 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 73
      }
      4410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 73
      }
      4439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 74
      }
      4445 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 74
      }
      4455 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 74
      }
      4470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 74
      }
      4499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 75
      }
      4505 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 75
      }
      4530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 75
      }
      4559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 76
      }
      4565 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 76
      }
      4590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 76
      }
      4619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 77
      }
      4625 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 77
      }
      4650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 77
      }
      4679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 78
      }
      4685 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 78
      }
      4710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 78
      }
      4739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 79
      }
      4745 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 79
      }
      4770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 79
      }
      4799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 80
      }
      4805 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 80
      }
      4823 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 80
      }
      4824 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 80
      }
      4825 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 80
      }
      4826 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 80
      }
      4827 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 80
      }
      4828 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 80
      }
      4829 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 80
      }
      4830 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 80
      }
      4859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 81
      }
      4865 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 81
      }
      4890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 81
      }
      4919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 82
      }
      4925 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 82
      }
      4950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 82
      }
      4979 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 83
      }
      4980 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 83
      }
      4981 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 83
      }
      4985 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 83
      }
      4986 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 83
      }
      4987 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 83
      }
      4988 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 83
      }
      4989 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 83
      }
      4990 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 83
      }
      5010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 83
      }
      5039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 84
      }
      5045 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 84
      }
      5070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 84
      }
      5099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 85
      }
      5105 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 85
      }
      5130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 85
      }
      5159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 86
      }
      5165 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 86
      }
      5190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 86
      }
      5219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 87
      }
      5225 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 87
      }
      5250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 87
      }
      5279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 88
      }
      5285 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 88
      }
      5310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 88
      }
      5339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 89
      }
      5345 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 89
      }
      5370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 89
      }
      5399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 90
      }
      5405 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 90
      }
      5430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 90
      }
      5459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 91
      }
      5465 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 91
      }
      5490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 91
      }
      5519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 92
      }
      5525 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 92
      }
      5550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 92
      }
      5579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 93
      }
      5585 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 93
      }
      5610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 93
      }
      5639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 94
      }
      5645 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 94
      }
      5670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 94
      }
      5699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 95
      }
      5705 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 95
      }
      5730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 95
      }
      5759 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 96
      }
      5760 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 96
      }
      5761 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 96
      }
      5765 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 96
      }
      5790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 96
      }
      5819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 97
      }
      5825 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 97
      }
      5826 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 97
      }
      5827 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 97
      }
      5828 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 97
      }
      5829 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 97
      }
      5830 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 97
      }
      5831 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 97
      }
      5832 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 97
      }
      5833 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 97
      }
      5834 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 97
      }
      5835 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 97
      }
      5850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 97
      }
      5879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 98
      }
      5885 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 98
      }
      5910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 98
      }
      5939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 99
      }
      5945 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 99
      }
      5970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 99
      }
      5999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 100
      }
      6005 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 100
      }
      6030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 100
      }
      6059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 101
      }
      6065 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 101
      }
      6090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 101
      }
      6119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 102
      }
      6125 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 102
      }
      6150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 102
      }
      6179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 103
      }
      6185 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 103
      }
      6205 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 103
      }
      6206 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 103
      }
      6207 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 103
      }
      6208 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 103
      }
      6209 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 103
      }
      6210 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 103
      }
      6239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 104
      }
      6245 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 104
      }
      6270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 104
      }
      6299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 105
      }
      6305 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 105
      }
      6330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 105
      }
      6359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 106
      }
      6365 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 106
      }
      6390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 106
      }
      6419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 107
      }
      6425 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 107
      }
      6450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 107
      }
      6479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 108
      }
      6485 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 108
      }
      6510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 108
      }
      6539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 109
      }
      6545 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 109
      }
      6570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 109
      }
      6599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 110
      }
      6605 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 110
      }
      6630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 110
      }
      6659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 111
      }
      6665 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 111
      }
      6690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 111
      }
      6719 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 112
      }
      6720 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 112
      }
      6724 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 112
      }
      6725 {
        :id 0
        :index [4 12 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 112
      }
      6731 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 112
      }
      6732 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 112
      }
      6733 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 112
      }
      6734 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 112
      }
      6735 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 112
      }
      6736 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 112
      }
      6737 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 112
      }
      6738 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 112
      }
      6739 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 112
      }
      6740 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 112
      }
      6741 {
        :id 0
        :index [8 8 3 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 112
      }
      6742 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 112
      }
      6743 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 112
      }
      6744 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 112
      }
      6745 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 112
      }
      6750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 112
      }
      6779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 113
      }
      6801 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 113
      }
      6810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 113
      }
      6839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 114
      }
      6861 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 114
      }
      6870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 114
      }
      6899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 115
      }
      6921 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 115
      }
      6930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 115
      }
      6959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 116
      }
      6981 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 116
      }
      6990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 116
      }
      7019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 117
      }
      7041 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 117
      }
      7050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 117
      }
      7079 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 118
      }
      7080 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 118
      }
      7081 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 118
      }
      7082 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 118
      }
      7083 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 118
      }
      7084 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 118
      }
      7085 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 118
      }
      7086 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 118
      }
      7087 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 118
      }
      7088 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 118
      }
      7089 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 118
      }
      7090 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 118
      }
      7091 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 118
      }
      7092 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 118
      }
      7093 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 118
      }
      7094 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 118
      }
      7095 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 118
      }
      7096 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 118
      }
      7097 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 118
      }
      7098 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 118
      }
      7099 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 118
      }
      7100 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 118
      }
      7101 {
        :id 0
        :index [4 12 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 118
      }
      7110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 118
      }
      7139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 119
      }
      7170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 119
      }
      7199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 120
      }
      7230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 120
      }
      7259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 121
      }
      7290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 121
      }
      7319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 122
      }
      7350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 122
      }
      7379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 123
      }
      7410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 123
      }
      7439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 124
      }
      7461 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 124
      }
      7462 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 124
      }
      7463 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 124
      }
      7464 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 124
      }
      7465 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 124
      }
      7466 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 124
      }
      7467 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 124
      }
      7468 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 124
      }
      7469 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 124
      }
      7470 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 124
      }
      7499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 125
      }
      7530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 125
      }
      7559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 126
      }
      7590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 126
      }
      7619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 127
      }
      7650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 127
      }
      7679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 128
      }
      7710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 128
      }
      7739 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 129
      }
      7770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 129
      }
      7799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 130
      }
      7830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 130
      }
      7859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 131
      }
      7890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 131
      }
      7919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 132
      }
      7950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 132
      }
      7979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 133
      }
      8010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 133
      }
      8039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 134
      }
      8070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 134
      }
      8099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 135
      }
      8130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 135
      }
      8159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 136
      }
      8190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 136
      }
      8219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 137
      }
      8250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 137
      }
      8279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 138
      }
      8310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 138
      }
      8339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 139
      }
      8370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 139
      }
      8399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 140
      }
      8412 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 140
      }
      8430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 140
      }
      8459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 141
      }
      8472 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 141
      }
      8473 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 141
      }
      8474 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 141
      }
      8475 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 141
      }
      8476 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 141
      }
      8477 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 141
      }
      8478 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 141
      }
      8490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 141
      }
      8519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 142
      }
      8532 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 142
      }
      8550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 142
      }
      8579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 143
      }
      8592 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 143
      }
      8610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 143
      }
      8639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 144
      }
      8652 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 144
      }
      8670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 144
      }
      8699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 145
      }
      8712 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 145
      }
      8730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 145
      }
      8759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 146
      }
      8772 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 146
      }
      8790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 146
      }
      8819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 147
      }
      8832 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 147
      }
      8850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 147
      }
      8879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 148
      }
      8892 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 148
      }
      8910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 148
      }
      8939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 149
      }
      8952 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 149
      }
      8970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 149
      }
      8999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 150
      }
      9004 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 150
      }
      9005 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 150
      }
      9006 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 150
      }
      9007 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 150
      }
      9008 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 150
      }
      9009 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 150
      }
      9010 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 150
      }
      9011 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 150
      }
      9012 {
        :id 0
        :index [4 12 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 150
      }
      9024 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 150
      }
      9025 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 150
      }
      9026 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 150
      }
      9027 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 150
      }
      9028 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 150
      }
      9029 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 150
      }
      9030 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 150
      }
      9059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 151
      }
      9090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 151
      }
      9119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 152
      }
      9150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 152
      }
      9179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 153
      }
      9210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 153
      }
      9239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 154
      }
      9270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 154
      }
      9299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 155
      }
      9330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 155
      }
      9359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 156
      }
      9390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 156
      }
      9419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 157
      }
      9429 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 157
      }
      9430 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 157
      }
      9431 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 157
      }
      9432 {
        :id 0
        :index [8 8 3 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 157
      }
      9433 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 157
      }
      9434 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 157
      }
      9435 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 157
      }
      9436 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 157
      }
      9437 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 157
      }
      9438 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 157
      }
      9439 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 157
      }
      9440 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 157
      }
      9441 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 157
      }
      9450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 157
      }
      9479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 158
      }
      9492 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 158
      }
      9510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 158
      }
      9539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 159
      }
      9552 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 159
      }
      9570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 159
      }
      9599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 160
      }
      9612 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 160
      }
      9630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 160
      }
      9659 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 161
      }
      9660 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 161
      }
      9661 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 161
      }
      9662 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 161
      }
      9672 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 161
      }
      9690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 161
      }
      9719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 162
      }
      9732 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 162
      }
      9750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 162
      }
      9779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 163
      }
      9792 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 163
      }
      9810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 163
      }
      9839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 164
      }
      9852 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 164
      }
      9870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 164
      }
      9899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 165
      }
      9912 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 165
      }
      9930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 165
      }
      9959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 166
      }
      9972 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 166
      }
      9990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 166
      }
      10019 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 167
      }
      10020 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 167
      }
      10021 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 167
      }
      10022 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 167
      }
      10023 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 167
      }
      10024 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 167
      }
      10025 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 167
      }
      10032 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 167
      }
      10050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 167
      }
      10079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 168
      }
      10092 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 168
      }
      10110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 168
      }
      10139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 169
      }
      10152 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 169
      }
      10170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 169
      }
      10199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 170
      }
      10212 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 170
      }
      10230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 170
      }
      10259 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 171
      }
      10272 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 171
      }
      10290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 171
      }
      10319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 172
      }
      10332 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 172
      }
      10350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 172
      }
      10379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 173
      }
      10392 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 173
      }
      10410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 173
      }
      10439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 174
      }
      10449 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 174
      }
      10450 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 174
      }
      10451 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 174
      }
      10452 {
        :id 0
        :index [4 2 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 174
      }
      10453 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 174
      }
      10454 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 174
      }
      10455 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 174
      }
      10456 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 174
      }
      10470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 174
      }
      10499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 175
      }
      10530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 175
      }
      10559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 176
      }
      10590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 176
      }
      10619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 177
      }
      10650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 177
      }
      10679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 178
      }
      10710 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 178
      }
      10739 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 179
      }
      10740 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 179
      }
      10741 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 179
      }
      10742 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 179
      }
      10743 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 179
      }
      10770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 179
      }
      10799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 180
      }
      10830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 180
      }
      10859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 181
      }
      10890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 181
      }
      10919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 182
      }
      10950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 182
      }
      10979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 183
      }
      11010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 183
      }
      11039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 184
      }
      11070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 184
      }
      11099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 185
      }
      11119 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 185
      }
      11120 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 185
      }
      11121 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 185
      }
      11122 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 185
      }
      11123 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 185
      }
      11124 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 185
      }
      11125 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 185
      }
      11126 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 185
      }
      11127 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 185
      }
      11128 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 185
      }
      11129 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 185
      }
      11130 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 185
      }
      11159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 186
      }
      11190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 186
      }
      11219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 187
      }
      11250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 187
      }
      11279 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 188
      }
      11310 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 188
      }
      11339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 189
      }
      11370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 189
      }
      11399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 190
      }
      11406 {
        :id 0
        :index [5 8 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 190
      }
      11407 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 190
      }
      11408 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 190
      }
      11409 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 190
      }
      11410 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 190
      }
      11411 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 190
      }
      11412 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 190
      }
      11430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 190
      }
      11459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 191
      }
      11466 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 191
      }
      11490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 191
      }
      11519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 192
      }
      11526 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 192
      }
      11542 {
        :id 0
        :index [5 8 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 192
      }
      11543 {
        :id 0
        :index [8 11 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 192
      }
      11550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 192
      }
      11579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 193
      }
      11586 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 193
      }
      11602 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 193
      }
      11603 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 193
      }
      11610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 193
      }
      11639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 194
      }
      11646 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 194
      }
      11662 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 194
      }
      11663 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 194
      }
      11670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 194
      }
      11699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 195
      }
      11722 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 195
      }
      11723 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 195
      }
      11730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 195
      }
      11759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 196
      }
      11782 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 196
      }
      11783 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 196
      }
      11790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 196
      }
      11819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 197
      }
      11842 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 197
      }
      11843 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 197
      }
      11850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 197
      }
      11879 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 198
      }
      11880 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 198
      }
      11881 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 198
      }
      11882 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 198
      }
      11883 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 198
      }
      11884 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 198
      }
      11885 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 198
      }
      11886 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 198
      }
      11887 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 198
      }
      11888 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 198
      }
      11889 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 198
      }
      11902 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 198
      }
      11903 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 198
      }
      11910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 198
      }
      11939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 199
      }
      11962 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 199
      }
      11963 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 199
      }
      11970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 199
      }
      11999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 200
      }
      12022 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 200
      }
      12023 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 200
      }
      12030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 200
      }
      12059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 201
      }
      12082 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 201
      }
      12083 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 201
      }
      12090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 201
      }
      12119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 202
      }
      12142 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 202
      }
      12143 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 202
      }
      12150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 202
      }
      12179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 203
      }
      12202 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 203
      }
      12203 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 203
      }
      12210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 203
      }
      12239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 204
      }
      12262 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 204
      }
      12263 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 204
      }
      12270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 204
      }
      12299 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 205
      }
      12322 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 205
      }
      12323 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 205
      }
      12330 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 205
      }
      12359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 206
      }
      12382 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 206
      }
      12383 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 206
      }
      12390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 206
      }
      12419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 207
      }
      12442 {
        :id 0
        :index [6 9 6 9]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 207
      }
      12443 {
        :id 0
        :index [9 12 9 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 207
      }
      12450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 207
      }
      12479 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 208
      }
      12480 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 208
      }
      12481 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 208
      }
      12482 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 208
      }
      12483 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 208
      }
      12484 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 208
      }
      12485 {
        :id 0
        :index [8 11 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 208
      }
      12492 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 208
      }
      12493 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 208
      }
      12494 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 208
      }
      12495 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 208
      }
      12496 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 208
      }
      12497 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 208
      }
      12498 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 208
      }
      12499 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 208
      }
      12500 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 208
      }
      12501 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 208
      }
      12502 {
        :id 0
        :index [4 9 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 208
      }
      12503 {
        :id 0
        :index [9 2 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 208
      }
      12504 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 208
      }
      12505 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 208
      }
      12506 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 208
      }
      12507 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 208
      }
      12508 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 208
      }
      12509 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 208
      }
      12510 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 208
      }
      12539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 209
      }
      12545 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 209
      }
      12570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 209
      }
      12599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 210
      }
      12605 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 210
      }
      12630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 210
      }
      12659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 211
      }
      12665 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 211
      }
      12690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 211
      }
      12719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 212
      }
      12725 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 212
      }
      12750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 212
      }
      12779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 213
      }
      12804 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 213
      }
      12805 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 213
      }
      12806 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 213
      }
      12807 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 213
      }
      12808 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 213
      }
      12809 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 213
      }
      12810 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 213
      }
      12839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 214
      }
      12870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 214
      }
      12899 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 215
      }
      12900 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 215
      }
      12901 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 215
      }
      12902 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 215
      }
      12903 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 215
      }
      12904 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 215
      }
      12905 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 215
      }
      12906 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 215
      }
      12907 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 215
      }
      12908 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 215
      }
      12909 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 215
      }
      12919 {
        :id 0
        :index [5 11 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 215
      }
      12930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 215
      }
      12959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 216
      }
      12979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 216
      }
      12990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 216
      }
      13019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 217
      }
      13039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 217
      }
      13050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 217
      }
      13079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 218
      }
      13099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 218
      }
      13110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 218
      }
      13139 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 219
      }
      13159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 219
      }
      13170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 219
      }
      13199 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 220
      }
      13219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 220
      }
      13230 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 220
      }
      13259 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 221
      }
      13260 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 221
      }
      13261 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 221
      }
      13262 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 221
      }
      13263 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 221
      }
      13264 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 221
      }
      13268 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 221
      }
      13269 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 221
      }
      13270 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 221
      }
      13271 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 221
      }
      13272 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 221
      }
      13273 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 221
      }
      13274 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 221
      }
      13275 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 221
      }
      13276 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 221
      }
      13277 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 221
      }
      13278 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 221
      }
      13279 {
        :id 0
        :index [4 2 3 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 221
      }
      13280 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 221
      }
      13281 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 221
      }
      13282 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 221
      }
      13290 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 221
      }
      13319 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 222
      }
      13339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 222
      }
      13350 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 222
      }
      13379 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 223
      }
      13399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 223
      }
      13410 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 223
      }
      13439 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 224
      }
      13459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 224
      }
      13470 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 224
      }
      13499 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 225
      }
      13519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 225
      }
      13530 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 225
      }
      13559 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 226
      }
      13579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 226
      }
      13590 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 226
      }
      13619 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 227
      }
      13639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 227
      }
      13650 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 227
      }
      13679 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 228
      }
      13699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 228
      }
      13706 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 228
      }
      13707 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 228
      }
      13708 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 228
      }
      13709 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 228
      }
      13710 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 228
      }
      13739 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 229
      }
      13740 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 229
      }
      13741 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 229
      }
      13742 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 229
      }
      13743 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 229
      }
      13744 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 229
      }
      13745 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 229
      }
      13746 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 229
      }
      13747 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 229
      }
      13748 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 229
      }
      13749 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 229
      }
      13750 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 229
      }
      13751 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 229
      }
      13759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 229
      }
      13770 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 229
      }
      13799 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 230
      }
      13819 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 230
      }
      13830 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 230
      }
      13859 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 231
      }
      13879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 231
      }
      13890 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 231
      }
      13919 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 232
      }
      13939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 232
      }
      13950 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 232
      }
      13979 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 233
      }
      13999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 233
      }
      14010 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 233
      }
      14039 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 234
      }
      14059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 234
      }
      14070 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 234
      }
      14099 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 235
      }
      14119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 235
      }
      14130 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 235
      }
      14159 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 236
      }
      14179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 236
      }
      14190 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 236
      }
      14219 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 237
      }
      14239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 237
      }
      14250 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 237
      }
      14279 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 238
      }
      14280 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 238
      }
      14281 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 238
      }
      14282 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 238
      }
      14283 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 238
      }
      14284 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 238
      }
      14285 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 238
      }
      14286 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 238
      }
      14294 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 238
      }
      14295 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 238
      }
      14296 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 238
      }
      14297 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 238
      }
      14298 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 238
      }
      14299 {
        :id 0
        :index [4 2 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 238
      }
      14300 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 238
      }
      14301 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 238
      }
      14302 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 238
      }
      14303 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 238
      }
      14304 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 238
      }
      14305 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 238
      }
      14306 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 238
      }
      14307 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 238
      }
      14308 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 238
      }
      14309 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 238
      }
      14310 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 238
      }
      14339 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 239
      }
      14370 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 239
      }
      14399 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 240
      }
      14430 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 240
      }
      14459 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 241
      }
      14490 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 241
      }
      14519 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 242
      }
      14550 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 242
      }
      14579 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 243
      }
      14610 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 243
      }
      14639 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 244
      }
      14670 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 244
      }
      14699 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 245
      }
      14730 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 245
      }
      14759 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 246
      }
      14790 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 246
      }
      14819 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 247
      }
      14820 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 247
      }
      14821 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 247
      }
      14822 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 247
      }
      14823 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 247
      }
      14824 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 247
      }
      14850 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 247
      }
      14879 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 248
      }
      14910 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 248
      }
      14939 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 249
      }
      14970 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 249
      }
      14999 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 250
      }
      15006 {
        :id 0
        :index [5 8 7 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 250
      }
      15007 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 250
      }
      15008 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 250
      }
      15009 {
        :id 0
        :index [8 11 10 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 250
      }
      15030 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 250
      }
      15059 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 251
      }
      15090 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 251
      }
      15119 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 252
      }
      15150 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 252
      }
      15179 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 253
      }
      15210 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 253
      }
      15239 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 254
      }
      15270 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 254
      }
      15299 {
        :id 0
        :index [6 2 6 1]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 255
      }
      15300 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 255
      }
      15301 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 255
      }
      15302 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 255
      }
      15303 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 255
      }
      15304 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 255
      }
      15305 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 255
      }
      15306 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 255
      }
      15307 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 255
      }
      15308 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 255
      }
      15309 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 255
      }
      15310 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 255
      }
      15311 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 255
      }
      15312 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 255
      }
      15313 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 255
      }
      15314 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 255
      }
      15315 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 15
        :y 255
      }
      15316 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 16
        :y 255
      }
      15317 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 17
        :y 255
      }
      15318 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 18
        :y 255
      }
      15319 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 19
        :y 255
      }
      15320 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 20
        :y 255
      }
      15321 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 21
        :y 255
      }
      15322 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 22
        :y 255
      }
      15323 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 23
        :y 255
      }
      15324 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 24
        :y 255
      }
      15325 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 25
        :y 255
      }
      15326 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 26
        :y 255
      }
      15327 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 27
        :y 255
      }
      15328 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 28
        :y 255
      }
      15329 {
        :id 0
        :index [8 8 10 10]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 29
        :y 255
      }
      15330 {
        :id 0
        :index [4 12 3 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 255
      }
      15359 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 256
      }
      15390 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 256
      }
      15419 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 257
      }
      15450 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 257
      }
      15479 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 258
      }
      15510 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 258
      }
      15539 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 259
      }
      15570 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 259
      }
      15599 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 260
      }
      15630 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 260
      }
      15659 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 261
      }
      15690 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 261
      }
      15719 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 262
      }
      15750 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 262
      }
      15779 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 263
      }
      15810 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 263
      }
      15839 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 264
      }
      15870 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 264
      }
      15899 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 265
      }
      15930 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 265
      }
      15959 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 266
      }
      15990 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 266
      }
      16019 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 267
      }
      16050 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 267
      }
      16079 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 268
      }
      16110 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 268
      }
      16139 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 269
      }
      16170 {
        :id 0
        :index [6 12 6 12]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 269
      }
      16199 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x -1
        :y 270
      }
      16230 {
        :id 0
        :index [6 12 7 13]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 30
        :y 270
      }
    }
    :objs {
      1930 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 10
        :y 32
      }
      2345 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 128
        :l "objs"
        :library "sprite"
        :sprite "end-gate"
        :type "gate"
        :w 16
        :x 5
        :y 39
      }
      2764 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 4
        :y 46
      }
      4860 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 0
        :y 81
      }
      6977 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 17
        :y 116
      }
      9310 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 10
        :y 155
      }
      11422 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 22
        :y 190
      }
      13150 {
        :flipped true
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 10
        :y 219
      }
      14183 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 23
        :y 236
      }
      15191 {
        :flipped false
        :h 32
        :id 0
        :image-h 224
        :image-w 704
        :l "objs"
        :library "sprite"
        :sprite "Shadow"
        :type "player"
        :w 32
        :x 11
        :y 253
      }
      15198 {
        :flipped false
        :h 32
        :id 0
        :image-h 32
        :image-w 832
        :l "objs"
        :library "sprite"
        :sprite "cat-outline"
        :type "cat"
        :w 32
        :x 18
        :y 253
      }
    }
    :sun {}
  }
  :height 34
  :id 0
  :name "level8"
  :next-level "level9"
  :tile-size 16
  :title "LEVEL 8"
  :water-rate 0.4
  :width 60
}
