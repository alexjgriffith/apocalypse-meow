(local tiny (require "lib.tiny"))

;; (local develop true)

(fn process [function-file name key]
  (if develop
      (fn [self entities dt]
        (let [function (. (require function-file) name key)]
          (function self entities dt)))
      (. (require function-file) name key)))

(fn compare [function]
   (if develop
          (fn [self e1 e2]
            (function e1 e2))
          function))

(fn filter [function]
  function)

(local utils {:process process
              :compare compare
              :filter filter})


(fn make-system [name type function-file filter? process? compare? no-activity?]
  ;; processingSystem sortedProcessingSystem
  (local system ((. tiny type)))
  (tset system :name name)
  (when filter?
    (tset system :filter
          (. (require function-file) name :filter)))
  (when compare?
    (tset system :compare
          (utils.compare (. (require function-file) name :compare))))
  (when process?
    (tset system :process
          (utils.process  function-file name :process)))
  (tset system :active (not no-activity?))
  ;; (tset system :active false)
  system)

;; each system is generated from tiny (normaily a proecssing system)
;; the details for each system including filters and process are
;; found in a functions- files
;; This information here is just boiler-plate to allow for
;; reloading when in development mode

;;; Update
(local editor (make-system :editor :processingSystem :functions-editor true true))

(local free-camera (make-system
                    :free-camera
                    :processingSystem
                    :functions-camera true true))

(local mid-camera (make-system
                    :mid-camera
                    :processingSystem
                    :functions-camera true true))


(local player-controler (make-system
                         :player-controler
                         :processingSystem
                         :functions-player-controler true true))

(local update-ui (make-system
                  :update-ui
                  :processingSystem
                  :functions-ui true true))

(local cat-system (make-system
                  :cat-system
                  :processingSystem
                  :functions-cat true true))

(local water-system (make-system
                     :water-system
                     :processingSystem
                     :functions-water true true))

(local gate-system (make-system
                     :gate-system
                     :processingSystem
                     :functions-gate true true))

;;; Draw
(local player (make-system :player :processingSystem :functions-draw true true true true))

(local tiles (make-system :tiles :processingSystem :functions-draw true true true true))

(local tiles-editor (make-system :tiles-editor :processingSystem :functions-draw true true true true))

(local draw-bump (make-system :draw-bump :processingSystem :functions-draw true true true true))

(local draw-ui (make-system :draw-ui :processingSystem :functions-draw true true true true))

(local draw-sprites (make-system :draw-sprites :processingSystem :functions-draw true true true true))

(local draw-player-sprite (make-system :draw-player-sprite :processingSystem :functions-draw true true true true))

(local draw-level-header-and-footer (make-system :draw-level-header-and-footer :processingSystem :functions-draw true true true true))

(local draw-water (make-system :draw-water :processingSystem :functions-draw true true true true))



(fn process-systems [ces type ...]
  (tset ces :callback type)
  (local orders (. ces type))
  (when orders
      (each [index system (pairs orders)]
        ((. system :update) system  [...]))
      (tset ces :callback :c-update)))

(fn setup-systems [ces systems type]
  (tset ces type {})
  (each [name system (pairs (. systems type))]
    (when (= system.world nil)
      (ces:addSystem system))
    (tset ces type name system)))

(fn remove-systems [ces systems type]
  (tset ces type {})
  ;; (each [name system (pairs (. systems type))]
  ;;   (when (= system.world nil)
  ;;     (ces:removeSystem system)))
  (tset ces type {}))


{: process-systems
 : setup-systems
 : remove-systems
 :editor
 {:update [editor free-camera cat-system update-ui]
  :draw [draw-level-header-and-footer tiles tiles-editor draw-sprites draw-player-sprite draw-bump draw-ui]
  :keypressed [editor]
  :keyreleased []
  :mousepressed [editor]
  :mousereleased [editor]
  :uicall []}
 :game
 {:update [water-system
           mid-camera
           cat-system
           gate-system
           player-controler
           update-ui]
  :draw [draw-level-header-and-footer
         tiles
         draw-sprites
         player
         draw-water
         draw-ui
         ]
  :keypressed []
  :keyreleased []
  :mousepressed []
  :mousereleased []}}
