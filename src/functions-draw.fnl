(local tiny (require "lib.tiny"))
(local params (require "params"))
(local map (require :map))

(local tiles {})
(local tiles-editor {})
(local player {})
(local draw-debug-player {})
(local draw-bump {})
(local draw-ui {})
(local draw-sprites {})
(local draw-player-sprite {})
(local draw-level-header-and-footer {})
(local draw-water {})


(tset player :filter (tiny.requireAll "render" "player"))
(tset draw-player-sprite :filter (tiny.requireAll "render" "player"))
(tset draw-debug-player :filter (tiny.requireAll "render" "player" "debug"))
(tset tiles :filter (tiny.requireAll "render" "tiles"))
(tset tiles-editor :filter (tiny.requireAll "render" "editor"))
(tset draw-bump :filter (tiny.requireAll "render" "bump-world"))
(tset draw-ui :filter (tiny.requireAll "render" "ui"))
(tset draw-sprites :filter (tiny.requireAll "render" "sprite"))
(tset draw-level-header-and-footer :filter (tiny.requireAll "render" "level" "tiles"))
(tset draw-water :filter (tiny.requireAll "water" "render"))

(fn player.process [self entity [camera]]
  (local image entity.image)
  ;; (let [animation (. entity.animations entity.anim) ]
  ;;   (animation.draw animation entity.image entity.pos.x entity.pos.y))
  (camera:draw
   (fn [l t w h]
     (local player-sprite-details (require :player-sprite-details))
     (local {: flipped : state
             :beam-offset [beam-offset-x beam-offset-y]
             : frame-offset
             :frame [i j]}
            entity.animation)
     ;; (local frame-offset 21)
     (local [x y] [entity.pos.x entity.pos.y])
     (local [mxp myp] (cameraGetPosition self) )
     ;; (local [mxp myp] [(+ (/ mx 2) l) (+ (/ my 2) t)])
     (local [hook-x hook-y]
            (match state
              :hook [(+ entity.hook-block.x (/ entity.hook-block.w 2))
                     (+ entity.hook-block.y (/ entity.hook-block.h 2))]
              _ [mxp myp]))

     (local [w2 h2] [(/ entity.size.w 2) (/ entity.size.h 2)])
     (love.graphics.setColor params.colours.light-yellow)
     ;; these quads should only be generated once at generation and then indexed
     ;; using i and j!.
     (local body-quad (. entity.quads (+ frame-offset i) 1))
     (local arm-quad (. entity.quads (+ frame-offset i) (+ j 1)))
     ;; Draw Body
     (love.graphics.setColor 1 1 1 1)
     (love.graphics.draw image body-quad
                         (math.floor (- x (* flipped 12) (if (= -1 flipped) -8 0)))
                         (math.floor (- y 24))
                         0 flipped 1)

     ;; draw beam
     (local [r g b a] params.colours.white)
     (love.graphics.setColor r g b
                             (if (= :hook entity.state-machine.current-state) 1
                                  entity.hook-block 0.5
                                  0.1))


     (when draw-beam
       (love.graphics.line (+ entity.pos.x w2 beam-offset-x)
                           (+ entity.pos.y h2 beam-offset-y)
                           hook-x hook-y))

          ;; Draw Arm
     (love.graphics.setColor 1 1 1 1)
     (love.graphics.draw image arm-quad
                         (math.floor (- x (* flipped 12) (if (= -1 flipped) -8 0)))
                         (math.floor (- y 24))
                         0 flipped 1)

     ;; Draw Block
     (love.graphics.setColor params.colours.white)
     (when draw-beam
     (when entity.hook-block
       (let [{: x : y : h : w} entity.hook-block]
         (love.graphics.rectangle "fill" x y h w))))x
     ;;(love.graphics.rectangle "line" x y entity.size.w entity.size.h)
     ))
  ;;(love.graphics.pop)
  )

(fn draw-player-sprite.process [self entity [camera]]
  (local image assets.sprites.Shadow)
  (camera:draw
   (fn [l t w h]
     (local player-sprite-details (require :player-sprite-details))
     (local {: flipped} entity.animation)
     (local [x y] [entity.pos.x entity.pos.y])
     (local body-quad (. entity.quads 1 1))
     (local arm-quad (. entity.quads 1 2))
     (love.graphics.setColor 1 1 1 1)
     (love.graphics.draw image body-quad
                         (math.floor (- x (* flipped 12) (if (= -1 flipped) -8 0)))
                         (math.floor (- y 24))
                         0 flipped 1)
     (love.graphics.draw image arm-quad
                         (math.floor (- x (* flipped 12) (if (= -1 flipped) -8 0)))
                         (math.floor (- y 24))
                         0 flipped 1)

  )))


(fn draw-debug-player.process [self entity [camera]]
     ;; (let [animation (. entity.animations entity.anim) ]
  ;;   (animation.draw animation entity.image entity.pos.x entity.pos.y))
  (camera:draw (fn [l t w h]
                 (local (mx my) (wrappedMouseGetPosition))
                 (local mxp (+ (/ mx 2) l ))
                 (local myp (+ (/ my 2) t ))
                 (love.graphics.push "all")
                 (love.graphics.setColor params.colours.light-yellow)
                 (love.graphics.rectangle "fill" entity.pos.x entity.pos.y entity.size.w entity.size.h)
                 (local [w2 h2] [(/ entity.size.w 2) (/ entity.size.h 2)])
                 (love.graphics.setColor params.colours.white)
                 (love.graphics.line (+ entity.pos.x w2) (+ entity.pos.y h2) mxp myp)
                 (love.graphics.setColor params.colours.light-yellow)
                 (when entity.hook-block
                   (let [{: x : y : h : w} entity.hook-block]
                     (love.graphics.rectangle "fill" x y h w))
                   )
                 ))
     (love.graphics.pop))

(fn tiles.process [self entity [camera]]
  (self.world.camera:draw
   (fn [l t w h]
     (fn get-mouse-pos [self level]
       (map.xy-to-tile (cameraGetPosition self) level.tile-size))
     (local [x y] (get-mouse-pos self entity))
     ;; (love.graphics.rectangle "line" (* 16 (- x 0)) (* 16 (- y 0)) 16 16)

     ;;2160
     ;; (love.graphics.)
     ;;(pp "processing tiles")
     ;; ()
     ;; needed for hotload
     ;; (entity.tileset-batch:setTexture (. assets.sprites entity.sprite-key))
     (love.graphics.draw entity.tileset-batch)
     ;;(love.graphics.draw entity.fore-batch)
     ;;(entity.object-batch:setTexture (. assets.sprites entity.sprite-key))
     ;;(love.graphics.draw entity.object-batch)
     ;; when edit
     )))


(fn tiles-editor.process [self entity [camera]]
  (camera:draw
   (fn [l t w h]
     ;; draw hover
     ;; (db entity.level.tile-data)
     ;; (db (. entity.level.tile-data entity.brush.brush))
     (local tile (-> entity.level.tile-data
                     (lume.filter  (fn [tile] (= tile.name entity.brush.brush)))
                     (lume.first)))

     (match tile.tile-fun
       :rect (entity.level.hover-batch:setTexture (. assets.sprites entity.level.sprite-key))
       :square10 (entity.level.hover-batch:setTexture (. assets.sprites entity.level.sprite-key))
       :sprite (entity.level.hover-batch:setTexture (. assets.sprites tile.sprite)))
     ;; (db  tile)
     (love.graphics.draw entity.level.hover-batch))))

(fn draw-bump.process [self entity [camera]]
  (local bump-plugin (require :bump-plugin))
  (camera:draw
   (fn [l t w h]
     (bump-plugin.draw entity.collidables entity.world))))

(fn draw-ui.process [self entity [camera]]
  (local (w h flags) (love.window.getMode))
  (local {: fullscreen} flags)
  (local off-x (math.floor  (/ (- w params.screen-width) 2)))
  (local state (require :state))
  (local seconds-to-time (require :time))
  (local level (. state.levels state.level))
  (love.graphics.push "all")
  (love.graphics.setColor params.colours.white)
  (love.graphics.setFont entity.element-font.text)
  (love.graphics.printf entity.hud  (+ 5 off-x) (- h 30) w :left)
  (love.graphics.printf (.. "Time:" (seconds-to-time level.time)) off-x (- h 30) (- params.screen-width 5) :right)

  (love.graphics.setFont entity.element-font.save-declaration)
  ;;(love.graphics.printf entity.save-declaration 150 (- params.screen-height 50) 800 :right)

  (love.graphics.printf entity.save-declaration off-x 20 params.screen-width :center)
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.rectangle "fill" 0 0 off-x h)
  (love.graphics.rectangle "fill" (+ off-x params.screen-width) 0 off-x h)
  (love.graphics.pop)
  )

(local font-cat-name (assets.fonts.bulkypix 10))
(fn draw-sprites.process [self entity [camera]]
  (camera:draw
   (fn [l t w h]
     ;; d(pp (. entity.animations entity.state))
     ;; (love.graphics.setColor 1 1 1 1)
     ;; (let [{: x : y : w : h} entity.stand-collider]
     ;;   (love.graphics.rectangle "line" x y w h))

     ;; (love.graphics.setColor 1 0 0 1)
     ;; (let [{: x : y : w : h} entity.save-collider]
     ;;   (love.graphics.rectangle "line" x y w h))

     (love.graphics.setColor entity.mod-colour)
     (tset (. entity.animations entity.state) :flippedH entity.flipped)
     (when (and entity.name entity.alive (not entity.saved))
       ;; (font:setFilter "linear" "linear")
       (love.graphics.push "all")
       (love.graphics.setFont font-cat-name)
       (love.graphics.setColor params.colours.white)
       (love.graphics.printf entity.name (- entity.pos.x 130) entity.pos.y 300 :center)
       (love.graphics.pop)
       )

     (when (and (= entity.type :gate) (not entity.at-gate))
       (love.graphics.push "all")
         (love.graphics.setFont font-cat-name)
         (love.graphics.setColor params.colours.white)
         (love.graphics.printf entity.name (- entity.pos.x 140) (- entity.pos.y 20) 300 :center)
         (love.graphics.pop)
         )
     (when (and (= entity.type :gate)  entity.at-gate)
         (local [off-a off-b]  (if entity.choice-leave [10 0] [0 10]))
         (love.graphics.push "all")
         (love.graphics.setFont font-cat-name)
         (love.graphics.setColor params.colours.white)
         (love.graphics.rectangle "line" (- entity.pos.x 60) (- entity.pos.y 20 off-a) 50 32)
         (love.graphics.rectangle "line" (- entity.pos.x -20) (- entity.pos.y 20 off-b) 50 32)
         (love.graphics.printf "Next Level?" (- entity.pos.x 60) (- entity.pos.y 15 off-a) 50 :center)
         (love.graphics.printf "Save Cats?" (- entity.pos.x -20) (- entity.pos.y 15 off-b) 50 :center)
         (love.graphics.pop))

     (: (. entity.animations entity.state) :draw entity.image entity.pos.x entity.pos.y)
     )))

(local font-header ((. assets.fonts "operational amplifier") 80))
(font-header:setFilter :linear :nearest 1)
(fn draw-level-header-and-footer.process [self entity [camera]]
    (camera:draw
     (fn [l t w h]
       (love.graphics.setColor params.colours.white)
       (love.graphics.setFont font-header)
       (love.graphics.printf "Apocalypse Meow" 0 80 480 "center")
       (local font ((. assets.fonts "operational amplifier") 100))
       (font:setFilter :linear :nearest 1)
       (love.graphics.printf (or entity.level.title "") 0
                             (- params.map-height 200
                                (or entity.level.title-height 0)) 480 "center"))
  ))

(fn draw-water.process [self entity [camera]]
    (camera:draw
     (fn [l t w h]
       (love.graphics.push "all")
       (local [R G B _] params.colours.blue)
       (love.graphics.setColor [R G B 0.5])
       (love.graphics.rectangle "fill" 0 entity.level 480 entity.height)
       (love.graphics.pop))
  ))

{:player player :tiles tiles : draw-bump : draw-ui : draw-debug-player : tiles-editor : draw-sprites : draw-player-sprite : draw-level-header-and-footer : draw-water}
