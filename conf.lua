love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "apocalypse-meow", "apocalypse-meow"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 960
   t.window.height = 700--540
   t.window.vsync = 1
   t.window.x = 0
   t.window.y = 0
   t.window.resizable =false
   t.window.borderless=false
   t.window.fullscreentype = "desktop" -- exclusive   
   t.window.fullscreen = false
   t.window.display = 2
   t.version = "11.3"
   t.gammacorrect = true
end
