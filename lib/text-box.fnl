(local text-box {})

(local sample-text "The loaders option specifies how to load assets. Cargo uses filename extensions to determine how to load files. The keys of entries in the loaders table are the file extensions. These map to functions that take in a filename and return a loaded asset. In the above example, we run the function love.graphics.newImage on any filenames that end in .jpg")

(fn index [str index]
     (string.sub str index index))

(fn text-box.timer-greater? [self]
  (> self.timer self.time-per-character))

(fn text-box.less-than-length? [self]
  (<= self.index (string.len self.wrapped)))

(fn text-box.is-indexed-wordend? [self]
  (let [indexed (index self.wrapped self.index)]
    (or (= " " indexed ) (= "\n" indexed))))

(fn text-box.increment-string [self]
  (local char (index self.wrapped self.index))
  (when (= char "\n")
    (tset self :line (+ self.line 1)))
  (local peek-char (index self.wrapped (math.max (string.len self.wrapped)
                                            (+ 1 self.index))))
  (tset self :written (.. self.written char))
  (tset self :index (+ self.index 1))
  (tset self :timer 0)
  (values :letter char peek-char))

(fn text-box.end [self]
  (local to-reset {:timer 0 :running false})
  (each [key value (pairs to-reset)]
    (tset self key value))
  (values :end "\n" "\n"))

(fn text-box.pause [self]
  (local to-reset {:timer 0 :running false})
  (each [key value (pairs to-reset)]
    (tset self key value))
  (values :pause))

(fn text-box.reset [self]
  (local to-reset {:timer 0 :running false
                   :written "" :index 1})
  (each [key value (pairs to-reset)]
    (tset self key value))
  (values :reset))

(fn text-box.update [self dt]
  (when self.running
    (tset self :timer (+ self.timer dt))
    (match [(self:timer-greater?) (self:less-than-length?)]
      [true true] (self:increment-string)
      [_ false] (self:end)
      [_ _] :nothing)))

(fn text-box.start [self]
  (tset self :running true))

(fn text-box.skip [self]
  (while (and (not (self:is-indexed-wordend?))
              (self:less-than-length?))
    (self:increment-string))
  (tset self :timer 0)
  (values :skip))

(local text-box-mt
       {
        :__index text-box
        :update text-box.update
        :start text-box.start
        :skip text-box.skip
        :stop text-box.stop
        :sample-text sample-text
        })

(fn wrap-para [font str width]
  (let [(_ para-wrapped) (font:getWrap str width)]
    [(lume.reduce
      para-wrapped
      (fn [acc x] (.. acc "\n" x)))
     (# para-wrapped)]))

(fn wrap-string [font str width]
  ;; Font:getWrap( text, wraplimit )
  (->
   str
   (lume.split "\n")
   (lume.reduce
    (fn [[str count] substring]
      (let [[new-str lines] (text-box.wrap-para font substring width)]
        [(.. str "\n\n" new-str) (+ count lines)])
      )
    ["" 0]
    )
   ((fn [[str count]] [(string.sub str 3) count]))))

(fn [font str width height]
  (let [font-height (font:getHeight)
        [wrapped-string lines] (wrap-para font str width)
        box-height (* font-height lines)]
    (setmetatable
     {
      :wrapped wrapped-string
      :original str
      :width width
      :visible-height height
      :box-height box-height
      :font-height font-height
      :line 1
      :written ""
      :index 1
      :running false
      :time-per-character 0.02
      :timer 0
      }
     text-box-mt
     )
    )
  )
