(local json (require "lib.json"))
(local anim8 (require "lib.anim8"))

(fn get-durations [param from to]
    (let [ret {}]
      (for [i from to]
           (tset ret (+ 1 (# ret))
                 (/ (. param.frames (+ 1 i) "duration") 1000)))
      ret ))

(fn reverse [tab]
  (var ret [])
  (local max (# tab))
  (for [i 0 max]
    (table.insert ret (. tab (- max i)))
    )
  ret)

(fn loader [file width]
  (local param (. assets :data file))
  (local image (. assets :sprites file))
  (local grid (anim8.newGrid width 32 (: image :getWidth) (: image :getHeight)))
  (local animations {})
  (each [_ frame (ipairs param.meta.frameTags)]
    (match frame.direction
      :forward
      (tset animations frame.name
            (anim8.newAnimation (grid (.. (+ 1 frame.from) "-" (+ 1 frame.to)) 1)
                                (get-durations param frame.from frame.to)))
      :backward
      (tset animations frame.name
            (anim8.newAnimation (grid (.. (+ 1 frame.to) "-" (+ 1 frame.from)) 1)
                                (reverse (get-durations param frame.from frame.to))))
      ))
  {:animations animations :image image :grid grid :param param})
