(local timer {})

(fn timer.update [self dt]
  (if self.active
      (do
          (set self.time (+ self.time dt))
        (> self.time self.end))
      false))

(fn timer.over [self]
  (> self.time self.end))

(fn timer.reset [self]
  (set self.time 0))

(fn timer.change-end [self end]
  (set self.time 0)
  (set self.end end))

(fn timer.start [self]
  (set self.active true))

(fn timer.pause [self]
  (set self.active false))

(local timer-mt
       {:__index timer
        :over timer.over
        :update timer.update
        :reset timer.reset})

(fn new-timer [end start-paused?]
  (let [start-paused (or start-paused? false)]
    (setmetatable
     {:time 0
      :end end
      :active (not start-paused)}
     timer-mt)))
